# Oca Track Epak

Provides oca track epak shipping method

## Installation

Please use composer to install the extension.

    1. Add ssh public key to your bitbucket account.

    2. At your Magento root:

        * composer config repositories.oca-track-epak git git@giftment.git.beanstalkapp.com:/giftment/oca.git
        * composer require oca/track-epak
        * php bin/magento module:enable Oca_TrackEPak
        * php bin/magento setup:upgrade

## Changelog
    * 1.0.0 - Initial version
