<?php

namespace Oca\TrackEPak\Controller\Ajax;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Oca\TrackEPak\Model\Ws\Ms\Adapter;

/**
 * Catalog index page controller.
 */
class Branches extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Adapter
     */
    protected $adapter;

    public function __construct(
        JsonFactory  $resultJsonFactory,
        Context $context
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Branches action
     * [
     *  {
        "IdCentroImposicion": "94",
        "Sigla": "ROS",
        "Sucursal": "ROSARIO",
        "Calle": "SANTIAGO",
        "Numero": "380",
        "Torre": "",
        "Piso": "",
        "Localidad": "ROSARIO",
        "CodigoPostal": "2000",
        "Provincia": "SANTA FE",
        "Telefono": "0341-4378000",
        "TipoAgencia": "Sucursal OCA",
        "HorarioAtencion": "LUN A VIE 9 A 18 HS.",
        "SucursalOCA": "ROS"
        },
        {
         "IdCentroImposicion": "94",
         "Acronym": "ROS",
         "Branch": "ROSARIO",
         "Street": "SANTIAGO",
         "Number": "380",
         "Tower": "",
         "Floor": "",
         "Town": "ROSARIO",
         "Postcode": "2000",
         "Province": "SANTA FE",
         "Telephone": "0341-4378000",
         "TypeAgency": "OCA Branch",
         "Opening Hours": "MONDAY TO FRIDAY 9 TO 18 HS"
         "BranchOCA": "ROS"
     *  }
     * ]
     */
    public function execute()
    {
        // data json
        $result  = $this->resultJsonFactory->create();

        $this->adapter = $this->_objectManager->create(Adapter::class);
        $zipcode = $this->_request->getParam('zipcode');
        $branches = $this->adapter->execGetCentrosImposicionConServiciosByCP($zipcode);
        if (!is_array($branches)) {
            $branches = [];
        }

        return $result->setData($branches);
    }
}
