<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Shipment;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\Shipping\Oca as OcaShippingModel;
use Oca\TrackEPak\Model\Ws\Ms\Adapter;

/**
 * Class Save
 * @package Oca\TrackEPak\Controller\Adminhtml\Shipment
 */
class Save extends CommonController
{
    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var OcaShippingModel
     */
    protected $ocaShippingModel;

    /**
     * Save constructor.
     * @param OcaShippingModel $ocaShippingModel
     * @param Adapter $adapter
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OcaShippingModel $ocaShippingModel,
        Adapter $adapter,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->ocaShippingModel = $ocaShippingModel;
        $this->adapter = $adapter;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultRedirect = $this->resultRedirectFactory->create();
        $shipmentId = $params['shipment_id'];
        try {
            $result = $this->ocaShippingModel->createShipment($params);
            if (!$result) {
                $this->messageManager->addErrorMessage(__('Can not create OCA shipment'));
                return $resultRedirect->setPath('sales/shipment/view', ['shipment_id' => $shipmentId]);
            }

            if ($result['status'] == false) {
                $this->messageManager->addErrorMessage(__('Error when create OCA shipment, detail: %1', $result['message']));
                return $resultRedirect->setPath('sales/shipment/view', ['shipment_id' => $shipmentId]);
            }

            $this->messageManager->addSuccessMessage(__('Create OCA shipment success: %1', $result['message']));
            return $resultRedirect->setPath('sales/shipment/view', ['shipment_id' => $shipmentId]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error when create OCA shipment, detail: %1', $e->getMessage()));
            return $resultRedirect->setPath('sales/shipment/view', ['shipment_id' => $shipmentId]);
        }
    }
}
