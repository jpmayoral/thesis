<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Shipment;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\Ws\Ms\Adapter;

/**
 * Class Save
 * @package Oca\TrackEPak\Controller\Adminhtml\Shipment
 */
class GetCentrosImposicionConServiciosByCP extends CommonController
{
    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * GetCentrosImposicionConServiciosByCP constructor.
     * @param Adapter $adapter
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        Adapter $adapter,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->adapter = $adapter;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $zipcode = $this->getRequest()->getParam('zipcode');
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result = $this->adapter->execGetCentrosImposicionConServiciosByCP($zipcode);
        $resultJson->setData($result);
        return $resultJson;
    }
}
