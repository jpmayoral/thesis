<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Shipment;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\Shipping\Oca as OcaShippingModel;

/**
 * Class CancelOcaShipment
 * @package Oca\TrackEPak\Controller\Adminhtml\Shipment
 */
class CancelOcaShipment extends CommonController
{
    /**
     * @var OcaShippingModel
     */
    protected $ocaShippingModel;

    /**
     * CancelOcaShipment constructor.
     * @param OcaShippingModel $ocaShippingModel
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OcaShippingModel $ocaShippingModel,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->ocaShippingModel = $ocaShippingModel;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        try {
            $this->ocaShippingModel->cancelOcaShipment($shipmentId);
            $this->messageManager->addSuccessMessage(__('Cancel Oca Shipment success'));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage(__('Error when cancel Oca shipment. Detail %1', $e->getMessage()));
        }
        return $resultRedirect->setPath('sales/shipment/view', ['shipment_id' => $shipmentId]);
    }
}
