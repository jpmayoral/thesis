<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\OcaTrackEPakOperatoryFactory;
use Oca\TrackEPak\Model\OcaTrackEPakOperatoryRepository;

/**
 * Class Save
 * @package Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory
 */
class Save extends CommonController
{
    /**
     * @var OcaTrackEPakOperatoryFactory
     */
    protected $operatoryFactory;

    /**
     * @var OcaTrackEPakOperatoryRepository
     */
    protected $operatoryRepository;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param DataPersistorInterface $dataPersistor
     * @param OcaTrackEPakOperatoryRepository $operatoryRepository
     * @param OcaTrackEPakOperatoryFactory $operatoryFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        OcaTrackEPakOperatoryRepository $operatoryRepository,
        OcaTrackEPakOperatoryFactory $operatoryFactory
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->operatoryFactory = $operatoryFactory;
        $this->operatoryRepository = $operatoryRepository;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = 1;
            }
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            if (!$id) {
                $model = $this->operatoryFactory->create();
            } else {
                try {
                    $model = $this->operatoryRepository->get($id);
                } catch (\Exception $e) {
                    $this->messageManager->addError(__('This Operatory no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->operatoryRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the operatory.'));
                $this->dataPersistor->clear(Edit::DATA_PERSISTOR_ID);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the group.'));
            }

            $this->dataPersistor->set(Edit::DATA_PERSISTOR_ID, $model);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
