<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\OcaTrackEPakOperatoryRepository;

/**
 * Class Edit
 * @package Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory
 */
class Edit extends CommonController
{
    const DATA_PERSISTOR_ID = 'operatory';

    /**
     * @var OcaTrackEPakOperatoryRepository
     */
    protected $operatoryRepository;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param OcaTrackEPakOperatoryRepository $operatoryRepository
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        OcaTrackEPakOperatoryRepository $operatoryRepository,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->operatoryRepository = $operatoryRepository;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            try {
                $model = $this->operatoryRepository->get($id);
                $this->dataPersistor->set(self::DATA_PERSISTOR_ID, $model);
            } catch (\Exception $e) {
                $this->messageManager->addError(__($e->getMessage() . ' This operatory no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        $resultPage->getConfig()->getTitle()->prepend((isset($model) && $model->getId()) ? __('Edit Operatory: %1', $model->getName()) : __('New Operatory'));
        return $resultPage;
    }
}
