<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\OcaTrackEPakOperatoryRepository;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory\CollectionFactory;

/**
 * Class MassDelete
 * @package Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory
 */
class MassDelete extends CommonController
{
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var OcaTrackEPakOperatoryRepository
     */
    private $operatoryRepository;

    /**
     * MassDelete constructor.
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param OcaTrackEPakOperatoryRepository $operatoryRepository
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        Filter $filter,
        CollectionFactory $collectionFactory,
        OcaTrackEPakOperatoryRepository $operatoryRepository,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->operatoryRepository = $operatoryRepository;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $deleted = 0;
        foreach ($collection->getItems() as $item) {
            $this->operatoryRepository->delete($item->getEntityId());
            $deleted++;
        }

        if ($deleted) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $deleted)
            );
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/');
    }
}
