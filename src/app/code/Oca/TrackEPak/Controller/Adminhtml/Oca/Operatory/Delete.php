<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\OcaTrackEPakOperatoryRepository;

/**
 * Class Delete
 * @package Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory
 */
class Delete extends CommonController
{
    /**
     * @var OcaTrackEPakOperatoryRepository
     */
    protected $operatoryRepository;

    /**
     * Delete constructor.
     * @param OcaTrackEPakOperatoryRepository $operatoryRepository
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OcaTrackEPakOperatoryRepository $operatoryRepository,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->operatoryRepository = $operatoryRepository;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam(OcaTrackEPakOperatoryInterface::ENTITY_ID);
        if ($id) {
            try {
                $this->operatoryRepository->delete($id);
                $this->messageManager->addSuccessMessage(__('Delete operatory successfully'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}
