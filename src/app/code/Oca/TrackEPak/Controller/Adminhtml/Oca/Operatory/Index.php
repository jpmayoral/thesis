<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory;

use Magento\Backend\Model\View\Result\Page;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;

/**
 * Class Index
 * @package Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory
 */
class Index extends CommonController
{
    /**
     * Index action
     *
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Operatories'));
        return $resultPage;
    }
}
