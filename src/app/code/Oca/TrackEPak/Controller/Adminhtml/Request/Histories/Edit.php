<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Request\Histories;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory as OcaTrackEPakRequestHistoryResource;

/**
 * Class Edit
 * @package Oca\TrackEPak\Controller\Adminhtml\Request\Histories
 */
class Edit extends CommonController
{
    /**
     * @var OcaTrackEPakRequestHistoryResource
     */
    protected $requestHistoriesResource;

    /**
     * Edit constructor.
     * @param OcaTrackEPakRequestHistoryResource $requestHistoriesResource
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OcaTrackEPakRequestHistoryResource $requestHistoriesResource,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->requestHistoriesResource = $requestHistoriesResource;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return Page|ResponseInterface|Redirect|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            $urlRedirectData = $this->requestHistoriesResource->getById($id);
            if (empty($urlRedirectData)) {
                $this->messageManager->addErrorMessage(__('The requested raw does not exist'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*');
            } else {
                $this->registry->register('current_request_history', $id);
            }
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        $resultPage->getConfig()->getTitle()->prepend(__('Request history'));
        return $resultPage;
    }
}
