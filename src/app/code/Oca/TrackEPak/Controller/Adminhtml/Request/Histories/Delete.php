<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Request\Histories;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Oca\TrackEPak\Api\Data\OcaTrackEPakRequestHistoryInterface;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory as OcaTrackEPakRequestHistoryResource;

/**
 * Class Delete
 * @package Oca\TrackEPak\Controller\Adminhtml\Request\Histories
 */
class Delete extends CommonController
{
    /**
     * @var OcaTrackEPakRequestHistoryResource
     */
    protected $requestHistoriesResource;

    /**
     * Delete constructor.
     * @param OcaTrackEPakRequestHistoryResource $requestHistoriesResource
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OcaTrackEPakRequestHistoryResource $requestHistoriesResource,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->requestHistoriesResource = $requestHistoriesResource;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam(OcaTrackEPakRequestHistoryInterface::ENTITY_ID);
        if ($id) {
            try {
                $this->requestHistoriesResource->deleteById($id);
                $this->messageManager->addSuccessMessage(__('Delete request history success'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}
