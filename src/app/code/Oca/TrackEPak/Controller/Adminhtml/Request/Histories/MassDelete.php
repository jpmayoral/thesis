<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Request\Histories;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Oca\TrackEPak\Api\OcaTrackEPakRequestHistoryRepositoryInterface;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;
use Oca\TrackEPak\Model\OcaTrackEPakRequestHistory;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory\CollectionFactory;

/**
 * Class MassDelete
 * @package Oca\TrackEPak\Controller\Adminhtml\Request\Histories
 */
class MassDelete extends CommonController
{
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var OcaTrackEPakRequestHistoryRepositoryInterface
     */
    private $requestHistoryRepository;

    /**
     * MassDelete constructor.
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param OcaTrackEPakRequestHistoryRepositoryInterface $requestHistoryRepository
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        Filter $filter,
        CollectionFactory $collectionFactory,
        OcaTrackEPakRequestHistoryRepositoryInterface $requestHistoryRepository,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->requestHistoryRepository = $requestHistoryRepository;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $requestHistoryDeleted = 0;
        /** @var OcaTrackEPakRequestHistory $requestHistory */
        foreach ($collection->getItems() as $requestHistory) {
            $this->requestHistoryRepository->delete($requestHistory->getEntityId());
            $requestHistoryDeleted++;
        }

        if ($requestHistoryDeleted) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $requestHistoryDeleted)
            );
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }
}
