<?php

namespace Oca\TrackEPak\Controller\Adminhtml\Request\Histories;

use Magento\Backend\Model\View\Result\Page;
use Oca\TrackEPak\Controller\Adminhtml\Common as CommonController;

/**
 * Class Index
 * @package Oca\TrackEPak\Controller\Adminhtml\RequestHistories
 */
class Index extends CommonController
{
    /**
     * Index action
     *
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        $resultPage->getConfig()->getTitle()->prepend(__('Request histories'));
        return $resultPage;
    }
}
