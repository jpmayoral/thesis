<?php

namespace Oca\TrackEPak\Ui\Component\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory\CollectionFactory;

/**
 * Class OcaTrackEPakRequestHistoryDataProvider
 * @package Oca\TrackEPak\Ui\DataProvider
 */
class OcaTrackEPakRequestHistoryDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * @var array
     */
    protected $_requestHistoryCollectionFactory;

    /**
     * OcaTrackEPakRequestHistoryDataProvider constructor.
     * @param CollectionFactory $employeeCollectionFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        CollectionFactory $requestHistoryCollectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $requestHistoryCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->_loadedData[$item->getEntityId()] = $item->getData();
        }

        return $this->_loadedData;
    }
}
