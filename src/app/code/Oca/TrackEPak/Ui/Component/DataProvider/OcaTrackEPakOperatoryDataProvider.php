<?php

namespace Oca\TrackEPak\Ui\Component\DataProvider;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Oca\TrackEPak\Controller\Adminhtml\Oca\Operatory\Edit;
use Oca\TrackEPak\Model\OcaTrackEPakOperatory;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory\CollectionFactory;

/**
 * Class OcaTrackEPakOperatoryDataProvider
 * @package Oca\TrackEPak\Ui\Component\DataProvider
 */
class OcaTrackEPakOperatoryDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var \Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory\Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * OcaTrackEPakOperatoryDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $operatoryCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $operatoryCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $operatoryCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $item = $this->dataPersistor->get(Edit::DATA_PERSISTOR_ID);

        if ($item instanceof OcaTrackEPakOperatory && $item->getId()) {
            $this->loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear(Edit::DATA_PERSISTOR_ID);
        }

        return $this->loadedData;
    }
}
