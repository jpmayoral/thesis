<?php

namespace Oca\TrackEPak\Plugin\Magento\Checkout\Model;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement as BaseShippingInformationManagement;
use Magento\Quote\Model\QuoteRepository;
use Oca\TrackEPak\Helper\Config as ConfigHelper;
use Oca\TrackEPak\Service\GetShippingPrice;

/**
 * Plugin Shipping Information Management
 */
class ShippingInformationManagement
{
    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * ShippingInformationManagement constructor.
     * @param QuoteRepository $quoteRepository
     * @param ConfigHelper $config
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        ConfigHelper $config
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->configHelper = $config;
    }

    /**
     * Save address information.
     *
     * @param BaseShippingInformationManagement $subject
     * @param int $cartId
     * @param ShippingInformationInterface $addressInformation
     * @return array
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        BaseShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/oca.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('===== beforeSaveAddressInformation');

        $extAttributes = $addressInformation->getExtensionAttributes();
        if (!$extAttributes) {
            return [$cartId, $addressInformation];
        }

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        $ocaShippingData = $extAttributes->getOcaShippingData();
        try {
            $ocaShippingData = json_decode($ocaShippingData, true);
            $ocaShippingData['package'] = $this->getPackageInformation($quote);
            $ocaShippingData = json_encode($ocaShippingData);
        } catch (\Exception $e) {
            //ignore exception
        }

        $quote->setData('oca_shipping_type', $extAttributes->getOcaShippingType());
        $quote->setData('oca_shipping_data', $ocaShippingData);

        $logger->info('1 extension type ' . $extAttributes->getOcaShippingType());
        $logger->info('1 extension data ' . $ocaShippingData);

        $logger->info('QUOTE oca_shipping', [
            'quote_id' => $quote->getId(),
            'type' => $quote->getData('oca_shipping_type'),
            'data' => $quote->getData('oca_shipping_data')
        ]);

        return [$cartId, $addressInformation];
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    protected function getPackageInformation($quote)
    {
        $defaultHeight = $this->configHelper->getProductDimensionHeight();
        $defaultWidth = $this->configHelper->getProductDimensionWidth();
        $defaultLength = $this->configHelper->getProductDimensionLength();
        $result = [];
        $weightUnit = $this->configHelper->getWeightUnit();
        $weightRate = GetShippingPrice::LBS_TO_KGS_RATE;
        if ($weightUnit == 'kgs') {
            $weightRate = 1;
        }

        /** @var \Magento\Quote\Model\Quote $quote */
        $allItem = $quote->getAllVisibleItems();
        foreach ($allItem as $item) {
            if (!$item->getIsVirtual()) {
                $dimensionHeight = $item->getProduct()->getData('ts_dimensions_height') * GetShippingPrice::INCH_TO_METRE_RATE;
                $dimensionWidth = $item->getProduct()->getData('ts_dimensions_width') * GetShippingPrice::INCH_TO_METRE_RATE;
                $dimensionLength = $item->getProduct()->getData('ts_dimensions_length') * GetShippingPrice::INCH_TO_METRE_RATE;
                //check default value
                $dimensionHeight = ($dimensionHeight == 0) ? $defaultHeight : $dimensionHeight;
                $dimensionWidth = ($dimensionWidth == 0) ? $defaultWidth : $dimensionWidth;
                $dimensionLength = ($dimensionLength == 0) ? $defaultLength : $dimensionLength;

                $weight = $item->getWeight() * $weightRate;

                $result[] = [
                    "product_id" => $item->getProduct()->getId(),
                    "alto" => $dimensionHeight,   //height
                    "ancho" => $dimensionWidth,    //width
                    "largo" => $dimensionLength,   //length
                    "peso" => $weight,    //weight
                    "valor" => $item->getPrice(),       //value price
                    "cant" => $item->getQty() //qty
                ];
            }
        }

        return $result;
    }
}
