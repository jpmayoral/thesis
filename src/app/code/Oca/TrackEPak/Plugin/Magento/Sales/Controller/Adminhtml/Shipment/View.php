<?php

namespace Oca\TrackEPak\Plugin\Magento\Sales\Controller\Adminhtml\Shipment;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\Manager as MessageManager;
use Magento\Sales\Controller\Adminhtml\Shipment\View as BaseShipmentView;
use Oca\TrackEPak\Model\Shipping\Oca as OcaShippingModel;

/**
 * Class View
 * @package Oca\TrackEPak\Plugin\Magento\Sales\Controller\Adminhtml\Shipment
 */
class View
{
    /**
     * @var MessageManager
     */
    protected $messageManager;

    /**
     * @var OcaShippingModel
     */
    protected $ocaShippingModel;

    /**
     * View constructor.
     * @param MessageManager $messageManager
     * @param OcaShippingModel $ocaShippingModel
     */
    public function __construct(
        MessageManager $messageManager,
        OcaShippingModel $ocaShippingModel
    ) {
        $this->messageManager = $messageManager;
        $this->ocaShippingModel = $ocaShippingModel;
    }

    /**
     * @param BaseShipmentView $subject
     * @return array
     */
    public function beforeExecute($subject)
    {
        $shipmentId = $subject->getRequest()->getParam('shipment_id');
        if (!$this->ocaShippingModel->isOcaShipment($shipmentId)) {
            return [];
        }

        if (!$this->ocaShippingModel->isCreatedOcaShipment($shipmentId)) {
            return [];
        }

        try {
            $this->ocaShippingModel->syncTrackingNumbers($shipmentId);
            $this->messageManager->addSuccessMessage(__('Oca shipment have been sync tracking status success'));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage(__('Error when sync tracking number from OCA, detail %1', $e->getMessage()));
        }

        return [];
    }
}
