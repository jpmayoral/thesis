<?php

namespace Oca\TrackEPak\Plugin\Magento\Shipping\Block\Adminhtml;

use Magento\Framework\View\LayoutInterface;
use Magento\Shipping\Block\Adminhtml\View as BaseView;
use Oca\TrackEPak\Model\Shipping\Oca as OcaShippingModel;

/**
 * Class View
 * @package Oca\TrackEPak\Plugin\Magento\Shipping\Block\Adminhtml
 */
class View
{
    /**
     * @var OcaShippingModel
     */
    protected $ocaShippingModel;

    /**
     * View constructor.
     * @param OcaShippingModel $ocaShippingModel
     */
    public function __construct(
        OcaShippingModel $ocaShippingModel
    ) {
        $this->ocaShippingModel = $ocaShippingModel;
    }

    /**
     * @param BaseView $subject
     * @param LayoutInterface $layoutInterFace
     * @return array
     */
    public function beforeSetLayout($subject, $layoutInterFace)
    {
        $shipmentId = $subject->getShipment()->getEntityId();
        if (!$this->ocaShippingModel->isOcaShipment($shipmentId)) {
            return [$layoutInterFace];
        }

        if (!$this->ocaShippingModel->isCreatedOcaShipment($shipmentId)) {
            return [$layoutInterFace];
        }

        $message = __('Are you sure you want to cancel oca shipment?');
        $subject->addButton(
            'cancel_oca_shipment',
            [
                'label' => __('Cancel OCA shipment'),
                'class' => 'cancel_shipment',
                'onclick' =>  "deleteConfirm('" . $message . "', '" . $this->getCancelOcaShipmentUrl($subject) . "')"
            ]
        );

        return [$layoutInterFace];
    }

    /**
     * @param BaseView $subject
     * @return mixed
     */
    private function getCancelOcaShipmentUrl($subject)
    {
        return $subject->getUrl(
            'oca_track_epak/shipment/cancelOcaShipment',
            [
                'shipment_id' => $subject->getShipment()->getEntityId(),
            ]
        );
    }
}
