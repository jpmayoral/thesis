<?php

namespace Oca\TrackEPak\Api;

use Magento\Framework\Exception\LocalizedException;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;

/**
 * Interface OcaTrackEPakOperatoryRepositoryInterface
 * @package Oca\TrackEPak\Api
 */
interface OcaTrackEPakOperatoryRepositoryInterface
{
    /**
     * @param OcaTrackEPakOperatoryInterface $ocaTrackEPakOperatory
     * @return OcaTrackEPakOperatoryInterface
     */
    public function save(OcaTrackEPakOperatoryInterface $ocaTrackEPakOperatory);

    /**
     * @param int $operatoryId
     * @return OcaTrackEPakOperatoryInterface
     * @throws LocalizedException
     */
    public function get($operatoryId);

    /**
     * @param int $operatoryId
     * @throws LocalizedException
     */
    public function delete($operatoryId);
}
