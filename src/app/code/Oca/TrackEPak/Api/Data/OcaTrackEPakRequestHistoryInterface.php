<?php

namespace Oca\TrackEPak\Api\Data;

/**
 * Interface OcaTrackEPakRequestHistoryInterface
 * @package Oca\TrackEPak\Api\Data
 */
interface OcaTrackEPakRequestHistoryInterface
{
    const ENTITY_ID = 'entity_id';

    const STATUS = 'status';

    const REQUEST_LINK = 'request_link';

    const REQUEST_DATA = 'request_data';

    const RESPONSE_DATA = 'response_data';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**
     * Get entity id
     *
     * @return string
     */
    public function getEntityId();

    /**
     * Set entity id
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get request link
     *
     * @return string
     */
    public function getRequestLink();

    /**
     * Set request link
     *
     * @param string $requestLink
     * @return $this
     */
    public function setRequestLink($requestLink);

    /**
     * Get request data
     *
     * @return string
     */
    public function getRequestData();

    /**
     * Set request data
     *
     * @param string $requestData
     * @return $this
     */
    public function setRequestData($requestData);

    /**
     * Get response data
     *
     * @return string
     */
    public function getResponseData();

    /**
     * Set response data
     *
     * @param string $responseData
     * @return $this
     */
    public function setResponseData($responseData);

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updated at
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
