<?php

namespace Oca\TrackEPak\Api\Data;

/**
 * Interface OcaTrackEPakOperatoryInterface
 * @package Oca\TrackEPak\Api\Data
 */
interface OcaTrackEPakOperatoryInterface
{
    const ENTITY_ID = 'entity_id';

    const CODE = 'code';

    const NAME = 'name';

    const STATUS = 'status';

    const IS_INSURANCE = 'is_insurance';

    const IS_DEFAULT = 'is_default';

    const TYPE = 'type';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**
     * Get entity id
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Set entity id
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set Code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Method Name
     *
     * @return string
     */
    public function getName();

    /**
     * Set Method Name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get Is Default
     *
     * @return int
     */
    public function getIsDefault();

    /**
     * Set Is Default
     *
     * @param int $isDefault
     * @return $this
     */
    public function setIsDefault($isDefault);

    /**
     * Get Is Insurance
     *
     * @return int
     */
    public function getIsInsurance();

    /**
     * Set Is Insurance
     *
     * @param int $isInsurance
     * @return $this
     */
    public function setIsInsurance($isInsurance);

    /**
     * Get Operatory Type
     *
     * @return string
     */
    public function getType();

    /**
     * Set Operatory Type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type);

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updated at
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
