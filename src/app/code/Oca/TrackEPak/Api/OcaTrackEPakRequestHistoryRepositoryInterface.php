<?php

namespace Oca\TrackEPak\Api;

use Magento\Framework\Exception\LocalizedException;
use Oca\TrackEPak\Api\Data\OcaTrackEPakRequestHistoryInterface;

/**
 * Interface OcaTrackEPakRequestHistoryRepositoryInterface
 * @package Oca\TrackEPak\Api
 */
interface OcaTrackEPakRequestHistoryRepositoryInterface
{
    /**
     * @param OcaTrackEPakRequestHistoryInterface $ocaTrackEPakRequestHistory
     * @return OcaTrackEPakRequestHistoryInterface
     */
    public function save(OcaTrackEPakRequestHistoryInterface $ocaTrackEPakRequestHistory);

    /**
     * @param int $ocaTrackEPakRequestHistoryId
     * @return OcaTrackEPakRequestHistoryInterface
     * @throws LocalizedException
     */
    public function get($ocaTrackEPakRequestHistoryId);

    /**
     * @param int $ocaTrackEPakRequestHistoryId
     * @throws LocalizedException
     */
    public function delete($ocaTrackEPakRequestHistoryId);
}
