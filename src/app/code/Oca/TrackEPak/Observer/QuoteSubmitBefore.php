<?php

namespace Oca\TrackEPak\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Quote Submit Before
 */
class QuoteSubmitBefore implements ObserverInterface
{
    /**
     * Save data from quote to order
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/oca.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('===== QuoteSubmitBefore');

        $order = $observer->getEvent()->getData('order');
        $quote = $observer->getEvent()->getData('quote');

        $order->setData('oca_shipping_type', $quote->getData('oca_shipping_type'));
        $order->setData('oca_shipping_data', $quote->getData('oca_shipping_data'));

        $logger->info('ORDER oca_shipping', [
            'type' => $order->getData('oca_shipping_type'),
            'data' => $order->getData('oca_shipping_data')
        ]);
    }
}
