<?php

namespace Oca\TrackEPak\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\User\Model\ResourceModel\User as UserResource;
use Magento\User\Model\UserFactory;

/**
 * Class Config
 * @package Oca\TrackEPak\Helper
 */
class Config extends AbstractHelper
{
    const XML_PATH_OCA_TRACK_EPAK_SOAP_URL = 'carriers/ocashipping/soap_oca_service_url';

    const XML_PATH_OCA_TRACK_EPAK_ELOCKER_SOAP_URL = 'carriers/ocashipping/soap_oca_elocker_service_url';

    const XML_PATH_OCA_TRACK_EPAK_TRACKING_URL = 'carriers/ocashipping/tracking_url';

    const XML_PATH_OCA_TRACK_EPAK_CUIT = 'carriers/ocashipping/cuit';

    const XML_PATH_OCA_TRACK_EPAK_USER_NAME = 'carriers/ocashipping/username';

    const XML_PATH_OCA_TRACK_EPAK_PASSWORD = 'carriers/ocashipping/password';

    const XML_PATH_OCA_TRACK_EPAK_ACCOUNT = 'carriers/ocashipping/account';

    const XML_PATH_OCA_TRACK_EPAK_OPERATORY = 'carriers/ocashipping/operatory';

    const XML_PATH_OCA_TRACK_EPAK_TIME_FOR_RECEPTION = 'carriers/ocashipping/time_for_reception';

    const XML_PATH_OCA_TRACK_EPAK_CONTACT_ID = 'carriers/ocashipping/contact_id';

    const XML_PATH_OCA_SHIP_TO_APPLICABLE_COUNTRIES = 'carriers/ocashipping/isallowspecific';

    const XML_PATH_OCA_SHIP_TO_SPECIFIC_COUNTRIES = 'carriers/ocashipping/specificcountry';

    const XML_PATH_WEIGHT_UNIT = 'general/locale/weight_unit';

    const XML_PATH_WAREHOUSE_INFORMATION = 'warehouse/general/information';

    const XML_PATH_DIMENSION_WIDTH = 'dimension/product/width';

    const XML_PATH_DIMENSION_HEIGHT = 'dimension/product/height';

    const XML_PATH_DIMENSION_LENGTH = 'dimension/product/length';

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var UserResource
     */
    protected $userResource;

    /**
     * Config constructor.
     * @param UserResource $userResource
     * @param UserFactory $userFactory
     * @param Context $context
     * @param Json $serializer
     */
    public function __construct(
        UserResource $userResource,
        UserFactory $userFactory,
        Context $context,
        Json $serializer
    ) {
        $this->userResource = $userResource;
        $this->userFactory = $userFactory;
        $this->serializer = $serializer;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getOcaTrackEPakUserInfo()
    {
        $userName = $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_USER_NAME);
        $password = $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_PASSWORD);
        return [
            'username' => $userName,
            'password' => $password
        ];
    }

    /**
     * @return string
     */
    public function getOcaSoapUrl()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_SOAP_URL);
    }
    /**
     * @return string
     */
    public function getOcaElockerSoapUrl()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_ELOCKER_SOAP_URL);
    }

    /**
     * @return string
     */
    public function getCuit()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_CUIT);
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_ACCOUNT);
    }

    /**
     * @return string
     */
    public function getOperatory()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_OCA_TRACK_EPAK_OPERATORY);
    }

    /**
     * @return string
     */
    public function getWeightUnit()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_WEIGHT_UNIT,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return float
     */
    public function getProductDimensionWidth()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_DIMENSION_WIDTH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return float
     */
    public function getProductDimensionHeight()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_DIMENSION_HEIGHT,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return float
     */
    public function getProductDimensionLength()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_DIMENSION_LENGTH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * Warehouse information
     *
     * @return array
     */
    public function getWarehouseInformation()
    {
        $data = $this->scopeConfig->getValue(
            self::XML_PATH_WAREHOUSE_INFORMATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $warehouseInfo = [];
        try {
            $data = $this->serializer->unserialize($data);
            if (count($data)) {
                foreach ($data as $key => $val) {
                    $warehouseInfo[$val['key']] = $val['value'];
                }
            }
        } catch (\Exception $e) {
            return [];
        }

        return $warehouseInfo;
    }

    /**
     * Is ship to specific countries
     * False => no
     * Array => yes, list of specific countries
     *
     * @return array|bool
     */
    public function getShipToSpecificCountries()
    {
        $isSpecificCountries = $this->scopeConfig->getValue(
            self::XML_PATH_OCA_SHIP_TO_APPLICABLE_COUNTRIES,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );

        if (!$isSpecificCountries) {
            return false;
        }

        $data = $this->scopeConfig->getValue(
            self::XML_PATH_OCA_SHIP_TO_SPECIFIC_COUNTRIES,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );

        return explode(',', $data);
    }

    /**
     * @return mixed
     */
    public function getTimeForReception()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_OCA_TRACK_EPAK_TIME_FOR_RECEPTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getTrackingUrl()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_OCA_TRACK_EPAK_TRACKING_URL,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return array
     */
    public function getContactInfo()
    {
        $userId = $this->scopeConfig->getValue(
            self::XML_PATH_OCA_TRACK_EPAK_CONTACT_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $user = $this->userFactory->create();
        $this->userResource->load($user, $userId, 'user_id');
        return [
          'contact_name' => $user->getName(),
          'contact_email' => $user->getEmail()
        ];
    }
}
