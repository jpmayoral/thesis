<?php

namespace Oca\TrackEPak\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;
use Oca\TrackEPak\Api\OcaTrackEPakOperatoryRepositoryInterface;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory as OcaTrackEPakOperatoryResourceModel;

/**
 * Class OcaTrackEPakOperatoryRepository
 * @package Oca\TrackEPak\Model
 */
class OcaTrackEPakOperatoryRepository implements OcaTrackEPakOperatoryRepositoryInterface
{
    /**
     * @var OcaTrackEPakOperatoryFactory
     */
    protected $operatoryFactory;

    /**
     * @var OcaTrackEPakOperatoryResourceModel
     */
    protected $operatoryResourceModel;

    /**
     * OcaTrackEPakRepository constructor.
     * @param OcaTrackEPakOperatoryFactory $operatoryFactory
     * @param OcaTrackEPakOperatoryResourceModel $operatoryResourceModel
     */
    public function __construct(
        OcaTrackEPakOperatoryFactory $operatoryFactory,
        OcaTrackEPakOperatoryResourceModel $operatoryResourceModel
    ) {
        $this->operatoryFactory = $operatoryFactory;
        $this->operatoryResourceModel = $operatoryResourceModel;
    }

    /**
     * @inheritdoc
     */
    public function save(OcaTrackEPakOperatoryInterface $operatory)
    {
        try {
            $this->operatoryResourceModel->save($operatory);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $operatory;
    }

    /**
     * @inheritdoc
     */
    public function get($operatoryId)
    {
        /** @var OcaTrackEPakOperatory $operatoryModel */
        $operatoryModel = $this->operatoryFactory->create();
        $this->operatoryResourceModel->load($operatoryModel, $operatoryId);
        if (!$operatoryModel->getId()) {
            throw new NoSuchEntityException(__('Operatory object with id "%1" does not exist.', $operatoryId));
        }

        return $operatoryModel;
    }

    /**
     * @inheritdoc
     */
    public function delete($operatoryId)
    {
        $this->operatoryResourceModel->deleteById($operatoryId);
    }
}
