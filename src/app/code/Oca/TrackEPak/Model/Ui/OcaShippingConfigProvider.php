<?php

namespace Oca\TrackEPak\Model\Ui;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;
use Oca\TrackEPak\Helper\Config as ConfigHelper;
use Oca\TrackEPak\Model\Carrier\OcaShipping;
use Oca\TrackEPak\Model\OcaTrackEPakOperatory;

/**
 * Class OcaShippingConfigProvider
 * @package Oca\TrackEPak\Model\Ui
 */
class OcaShippingConfigProvider implements ConfigProviderInterface
{
    const BRANCH_INFO_KEYS = [
        "Sucursal" => "Sucursal",
        "Calle" => "Calle",
        "Numero" => "Numero",
        "Torre" => "Torre",
        "Piso" => "Piso",
        "Localidad" => "Localidad",
        "CodigoPostal" => "Codigo Postal",
        "Provincia" => "Provincia",
        "Telefono" => "Telefono",
        "TipoAgencia" => "Tipo Agencia",
        "HorarioAtencion" => "Horario Atencion"
    ];

    /**
     * @var OcaTrackEPakOperatory
     */
    protected $operatoryModel;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * OcaShippingConfigProvider constructor.
     * @param OcaTrackEPakOperatory $operatoryModel
     * @param ConfigHelper $config
     * @param Cart $cart
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        OcaTrackEPakOperatory $operatoryModel,
        ConfigHelper $config,
        Cart $cart,
        UrlInterface $urlBuilder
    ) {
        $this->operatoryModel = $operatoryModel;
        $this->cart = $cart;
        $this->urlBuilder = $urlBuilder;
        $this->configHelper = $config;
    }

    /**
     * @return string
     */
    public function getOcaShippingMethod()
    {
        return sprintf("%s_%s", OcaShipping::CARRIER_CODE, OcaShipping::CARRIER_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        /** @var OcaTrackEPakOperatoryInterface $toDoorItem */
        $toDoorItem = $this->operatoryModel->getDefaultToDoor();
        /** @var OcaTrackEPakOperatoryInterface $toBranchItem */
        $toBranchItem = $this->operatoryModel->getDefaultToBranch();

        $shipToSpecificCountries = $this->configHelper->getShipToSpecificCountries();

        $quote = $this->cart->getQuote();
        $branchInfo = '';
        $branchData = $branchList = [];
        try {
            $shippingData = json_decode($quote->getOcaShippingData(), true);
            if (isset($shippingData['shipping_address']) && count($shippingData['shipping_address'])) {
                $branchData = $shippingData['shipping_address'];
                foreach ($branchData as $key => $value) {
                    if (isset(self::BRANCH_INFO_KEYS[$key])) {
                        $branchInfo .= '<div><strong>' . self::BRANCH_INFO_KEYS[$key] . ':</strong> ' . $value . '</div>';
                    }
                }
            }
        } catch (\Exception $e) {
            $branchInfo = '';
        }

        $checkedDefault = $toDoorItem->getEntityId() ? 'to_my_door' : 'to_branch';
        if (!$toBranchItem->getEntityId()) {
            $checkedDefault = 'to_my_door';
        }

        if ($branchData) {
            $branchList = [
                [
                    'value' => json_encode($branchData),
                    'name' => $branchData['Sucursal'] . ' (' . $branchData['CodigoPostal'] . ')',
                ]
            ];
            $checkedDefault = 'to_branch';
        }

        $defaultCountry = 'AR';
        if (is_array($shipToSpecificCountries) && !in_array($defaultCountry, $shipToSpecificCountries)) {
            $defaultCountry = isset($shipToSpecificCountries[0]) ? $shipToSpecificCountries[0] : '';
        }

        return [
            'isToMyDoor' => $toDoorItem->getEntityId() ? true : false,
            'isToBranch' => $toBranchItem->getEntityId() ? true : false,
            'ocaShippingMethod' => $this->getOcaShippingMethod(),
            'getBranchUrl' => $this->urlBuilder->getUrl('ocatrackepak/ajax/branches'),
            'operativeToDoor' => $toDoorItem->getEntityId() ? $toDoorItem->getCode() : '',
            'operativeToBranch' => $toBranchItem->getEntityId() ? $toBranchItem->getCode() : '',
            'checkedDefault' => $checkedDefault,
            'shipToSpecificCountries' => $shipToSpecificCountries,
            'defaultCountry' => $defaultCountry,
            'branchList' => $branchList,
            'selectedValueBranchList' => count($branchData) ? json_encode($branchData) : '',
            'selectedBranchArray' => count($branchData) ? $branchData : null,
            'branchInfoKeys' => self::BRANCH_INFO_KEYS,
            'selectedBranch' => $branchInfo
        ];
    }
}
