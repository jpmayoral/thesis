<?php

namespace Oca\TrackEPak\Model;

use Magento\Framework\Data\Collection;
use Magento\Framework\Model\AbstractModel;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;
use Oca\TrackEPak\Model\Config\Source\Status;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory as OcaTrackEPakOperatoryResourceModel;

/**
 * Class OcaTrackEPakOperatory
 * @package Oca\TrackEPak\Model
 */
class OcaTrackEPakOperatory extends AbstractModel implements OcaTrackEPakOperatoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * @inheritDoc
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getIsDefault()
    {
        return $this->getData(self::IS_DEFAULT);
    }

    /**
     * @inheritDoc
     */
    public function setIsDefault($isDefault)
    {
        return $this->setData(self::IS_DEFAULT, $isDefault);
    }

    /**
     * @inheritDoc
     */
    public function getIsInsurance()
    {
        return $this->getData(self::IS_INSURANCE);
    }

    /**
     * @inheritDoc
     */
    public function setIsInsurance($isInsurance)
    {
        return $this->setData(self::IS_INSURANCE, $isInsurance);
    }

    /**
     * @inheritDoc
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Construct Function
     */
    protected function _construct()
    {
        $this->_init(OcaTrackEPakOperatoryResourceModel::class);
    }

    /**
     * @return mixed
     */
    public function getDefaultToDoor()
    {
        return $this->getCollection()
            ->addFieldToFilter(OcaTrackEPakOperatoryInterface::STATUS, Status::ACTIVE)
            ->addFilterToDoor()
            ->setOrder(OcaTrackEPakOperatoryInterface::IS_DEFAULT, Collection::SORT_ORDER_DESC)
            ->getFirstItem();
    }

    /**
     * @return mixed
     */
    public function getDefaultToBranch()
    {
        return $this->getCollection()
            ->addFieldToFilter(OcaTrackEPakOperatoryInterface::STATUS, Status::ACTIVE)
            ->addFilterToBranch()
            ->setOrder(OcaTrackEPakOperatoryInterface::IS_DEFAULT, Collection::SORT_ORDER_DESC)
            ->getFirstItem();
    }
}
