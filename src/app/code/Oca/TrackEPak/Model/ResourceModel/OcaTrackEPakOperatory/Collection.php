<?php
namespace Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;
use Oca\TrackEPak\Model\Config\Source\OperatoryType;
use Oca\TrackEPak\Model\OcaTrackEPakOperatory as OcaTrackEPakOperatoryModel;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory as OcaTrackEPakOperatoryResourceModel;

/**
 * Class Collection
 * @package Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakOperatory
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = OcaTrackEPakOperatoryInterface::ENTITY_ID;

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        $this->_init(OcaTrackEPakOperatoryModel::class, OcaTrackEPakOperatoryResourceModel::class);
    }

    /**
     * Filter to Door
     *
     * @return Collection
     */
    public function addFilterToDoor()
    {
        return $this->addFieldToFilter(OcaTrackEPakOperatoryInterface::TYPE, ['in' => OperatoryType::GROUP_TO_DOOR]);
    }

    /**
     * Filter to Branch
     *
     * @return Collection
     */
    public function addFilterToBranch()
    {
        return $this->addFieldToFilter(OcaTrackEPakOperatoryInterface::TYPE, ['in' => OperatoryType::GROUP_TO_BRANCH]);
    }
}
