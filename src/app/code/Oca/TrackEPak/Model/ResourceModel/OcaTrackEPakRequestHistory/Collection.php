<?php
namespace Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Oca\TrackEPak\Api\Data\OcaTrackEPakRequestHistoryInterface;
use Oca\TrackEPak\Model\OcaTrackEPakRequestHistory as OcaTrackEPakRequestHistoryModel;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory as OcaTrackEPakRequestHistoryResourceModel;

/**
 * Class Collection
 * @package Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = OcaTrackEPakRequestHistoryInterface::ENTITY_ID;

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        $this->_init(OcaTrackEPakRequestHistoryModel::class, OcaTrackEPakRequestHistoryResourceModel::class);
    }
}
