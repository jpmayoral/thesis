<?php
namespace Oca\TrackEPak\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Oca\TrackEPak\Api\Data\OcaTrackEPakRequestHistoryInterface;
use Oca\TrackEPak\Setup\InstallSchema;

/**
 * Class OcaTrackEPakRequestHistory
 * @package Oca\TrackEPak\Model\ResourceModel
 */
class OcaTrackEPakRequestHistory extends AbstractDb
{
    /**
     * Resource initialisation
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(InstallSchema::TABLE_NAME, OcaTrackEPakRequestHistoryInterface::ENTITY_ID);
    }

    /**
     * @param $id
     * @throws LocalizedException
     */
    public function deleteById($id)
    {
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['entity_id = ?' => $id]
        );
    }
    /**
     * @param $ids
     * @throws LocalizedException
     */
    public function deleteByIds($ids)
    {
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['entity_id = in (?)' => $ids]
        );
    }

    /**
     * @param int $id
     * @return array
     * @throws LocalizedException
     */
    public function getById($id)
    {
        $select = $this->getConnection()->select()
            ->from($this->getMainTable())
            ->where(OcaTrackEPakRequestHistoryInterface::ENTITY_ID . ' = ?', $id)
            ->limit(1);
        return $this->getConnection()->fetchRow($select);
    }

    /**
     * @param string $status
     * @param string $requestLink
     * @param array $requestData
     * @param array $responseData
     * @throws LocalizedException
     */
    public function insertRequestLog($status, $requestLink, $requestData, $responseData)
    {
        $this->getConnection()->insert(
            $this->getMainTable(),
            [
                'status' => $status,
                'request_link' => $requestLink,
                'request_data' => $requestData,
                'response_data' => json_encode($responseData)
            ]
        );
    }
}
