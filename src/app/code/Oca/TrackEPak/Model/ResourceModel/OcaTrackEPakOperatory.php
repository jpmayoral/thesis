<?php
namespace Oca\TrackEPak\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class OcaTrackEPakOperatory
 * @package Oca\TrackEPak\Model\ResourceModel
 */
class OcaTrackEPakOperatory extends AbstractDb
{
    const TABLE_NAME = 'oca_track_epak_operatory';
    const ENTITY_ID = 'entity_id';

    /**
     * Resource initialisation
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::ENTITY_ID);
    }

    /**
     * @param $id
     * @throws LocalizedException
     */
    public function deleteById($id)
    {
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['entity_id = ?' => $id]
        );
    }
    /**
     * @param $ids
     * @throws LocalizedException
     */
    public function deleteByIds($ids)
    {
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['entity_id = in (?)' => $ids]
        );
    }
}
