<?php

namespace Oca\TrackEPak\Model\Config\Source;

/**
 * Class Flatrate
 * @package Oca\TrackEPak\Model\Config\Source
 */
class Flatrate
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => __('None')],
            ['value' => 'O', 'label' => __('Per Order')],
            ['value' => 'I', 'label' => __('Per Item')]
        ];
    }
}
