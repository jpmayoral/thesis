<?php

namespace Oca\TrackEPak\Model\Config\Source;

/**
 * Class Status
 * @package Oca\TrackEPak\Model\Config\Source
 */
class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    const ACTIVE = 1;
    const INACTIVE = 0;

    /**
     * Options array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value' => self::INACTIVE, 'label' => __('Inactive')],
            ['value' => self::ACTIVE, 'label' => __('Active')],
        ];

        return $options;
    }
}
