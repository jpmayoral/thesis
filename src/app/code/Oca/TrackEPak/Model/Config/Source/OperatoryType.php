<?php

namespace Oca\TrackEPak\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Config Source Operatory Type
 */
class OperatoryType extends \Magento\Framework\DataObject implements OptionSourceInterface
{
    const TYPE_DOOR_TO_DOOR = 1;
    const TYPE_BRANCH_TO_DOOR = 2;
    const TYPE_DOOR_TO_BRANCH = 3;
    const TYPE_BRANCH_TO_BRANCH = 4;
    const GROUP_TO_DOOR = [
        self::TYPE_DOOR_TO_DOOR,
        self::TYPE_BRANCH_TO_DOOR,
    ];
    const GROUP_TO_BRANCH = [
        self::TYPE_DOOR_TO_BRANCH,
        self::TYPE_BRANCH_TO_BRANCH,
    ];
    const LABEL_TYPES = [
        self::TYPE_DOOR_TO_DOOR     => 'Door to Door',
        self::TYPE_BRANCH_TO_DOOR   => 'Branch to Door',
        self::TYPE_DOOR_TO_BRANCH   => 'Door to Branch',
        self::TYPE_BRANCH_TO_BRANCH => 'Branch to Branch',
    ];

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options = [];
        foreach (self::LABEL_TYPES as $type => $label) {
            $options[] = ['value' => $type, 'label' => __($label)];
        }

        return $options;
    }
}
