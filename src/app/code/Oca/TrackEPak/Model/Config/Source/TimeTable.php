<?php

namespace Oca\TrackEPak\Model\Config\Source;

/**
 * Class TimeTable
 * @package Oca\TrackEPak\Model\Config\Source
 */
class TimeTable
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '1',
                'label' => __('08:00 to 17:00')
            ],
            [
                'value' => '2',
                'label' => __('08:00 to 12:00')
            ],
            [
                'value' => '3',
                'label' => __('14:00 to 17:00')
            ],
        ];
    }
}
