<?php

namespace Oca\TrackEPak\Model\Config\Source;

use Magento\User\Model\ResourceModel\User\Collection;

/**
 * Class Contact
 * @package Oca\TrackEPak\Model\Config\Source
 */
class Contact
{
    /**
     * @var Collection
     */
    protected $adminUserCollection;

    /**
     * Contact constructor.
     * @param Collection $adminUserCollection
     */
    public function __construct(
        Collection $adminUserCollection
    ) {
        $this->adminUserCollection = $adminUserCollection;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $results = [];
        if ($this->adminUserCollection->count() > 0) {
            foreach ($this->adminUserCollection as $item) {
                $results[] = ['value' => $item->getData('user_id') , 'label' => $item->getData('username')];
            }
        }

        return $results;
    }
}
