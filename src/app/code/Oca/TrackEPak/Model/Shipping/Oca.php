<?php

namespace Oca\TrackEPak\Model\Shipping;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Api\Data\ShipmentItemInterface;
use Magento\Sales\Api\Data\ShipmentTrackInterface;
use Magento\Sales\Api\Data\ShipmentTrackInterfaceFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Oca\TrackEPak\Helper\Config as ConfigHelper;
use Oca\TrackEPak\Model\Carrier\OcaShipping;
use Oca\TrackEPak\Model\Ws\Ms\Adapter;
use SoapFault;

/**
 * Class Oca
 * @package Oca\TrackEPak\Model\Shipping
 */
class Oca extends AbstractModel
{
    const OCA_SHIPPING_METHOD = 'ocashipping_ocashipping';

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var ShipmentRepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var ShipmentTrackInterfaceFactory
     */
    protected $shipmentTrackInterfaceFactory;

    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * Oca constructor.
     * @param ConfigHelper $configHelper
     * @param CartRepositoryInterface $quoteRepository
     * @param Adapter $adapter
     * @param OrderRepositoryInterface $orderRepository
     * @param ShipmentTrackInterfaceFactory $shipmentTrackInterfaceFactory
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        ConfigHelper $configHelper,
        CartRepositoryInterface $quoteRepository,
        Adapter $adapter,
        OrderRepositoryInterface $orderRepository,
        ShipmentTrackInterfaceFactory $shipmentTrackInterfaceFactory,
        ShipmentRepositoryInterface $shipmentRepository,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->configHelper = $configHelper;
        $this->quoteRepository = $quoteRepository;
        $this->adapter = $adapter;
        $this->orderRepository = $orderRepository;
        $this->shipmentTrackInterfaceFactory = $shipmentTrackInterfaceFactory;
        $this->shipmentRepository = $shipmentRepository;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param array $params
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws SoapFault
     */
    public function createShipment($params)
    {
        $shipmentId = $params['shipment_id'];
        $shipment = $this->getShipment($shipmentId);
        $shipmentItems = $shipment->getItems();
        $order = $this->orderRepository->get($shipment->getOrderId());
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $operaty = $this->getQuoteOperaty($quote);
        $shippingSourceType = $params['shipping_type'];
        $shippingDesType = $this->getShippingType($quote);
        if ($shippingSourceType == 'from_branch') {
            $shippingAddressSource = $this->prepareForFromBranch($params);
        } else {
            $shippingAddressSource = $this->prepareForFromDoor($params);
        }

        if ($shippingDesType == 1) {
            $shippingAddressDes = $this->prepareDataForToDoor($quote);
        } else {
            $shippingAddressDes = $this->prepareDataForToBranch($quote);
        }

        $quotePackages = $this->getQuotePackages($quote);
        $xmlDataPost = $this->createXmlDataPostToCreateShipment($operaty, $shippingAddressSource, $shippingAddressDes, $quotePackages, $shipmentItems);
        $userInfo = $this->configHelper->getOcaTrackEPakUserInfo();
        $arrayData = [
            'usr' => $userInfo['username'],
            'psw' => $userInfo['password'],
            'xml_Datos' => $xmlDataPost,
            'ConfirmarRetiro' => true,
            'ArchivoCliente' => '',
            'ArchivoProceso' => ''
        ];
        return $this->adapter->createShipment($shipmentId, $arrayData);
    }

    /**
     * @param array $params
     * @return array
     */
    private function prepareForFromDoor($params)
    {
        return [
            'nroremito' => $params['nroremito_from_door'],
            'calleRetiro' => $params['calleRetiro'],
            'nroRetiro' => $params['nroRetiro'],
            'cpRetiro' => $params['cpRetiro'],
            'localidadRetiro' => $params['localidadRetiro'],
            'provinciaRetiro' => $params['provinciaRetiro'],
            'contactoRetiro' => $params['contactoRetiro'],
            'emailRetiro' => $params['emailRetiro'],
            'solicitanteRetiro' => $params['contactoRetiro'],
            'observaciones' => "",  // empty for now
            'centrocosto' => "", // here goes the value that is returned by getcentroCostoPorOperativa
            'idfranjahoraria' => $this->configHelper->getTimeForReception(),
            'IDCIOrigen' => "0",
            'fecharetiro' => date('Ymd')
        ];
    }

    /**
     * @param array $params
     * @return array
     */
    private function prepareForFromBranch($params)
    {
        $branchData = json_decode($params["branch_data"]);
        $contactInfo = $this->configHelper->getContactInfo();
        return [
            'nroremito' => $params['nroremito_from_branch'],
            'calleRetiro' => $branchData->Calle,
            'nroRetiro' => $branchData->Numero,
            'cpRetiro' => $branchData->CodigoPostal,
            'localidadRetiro' => $branchData->Localidad,
            'provinciaRetiro' => $branchData->Provincia,
            'contactoRetiro' => $contactInfo['contact_name'],
            'emailRetiro' => $contactInfo['contact_email'],
            'solicitanteRetiro' => $contactInfo['contact_name'], // same as contact name
            'observaciones' => "", // empty for now
            'centrocosto' => "", // here goes the value that is returned by getcentroCostoPorOperativa
            'idfranjahoraria' => $this->configHelper->getTimeForReception(),
            'IDCIOrigen' => $branchData->IdCentroImposicion,
            'fecharetiro' => date('Ymd'),
        ];
    }

    /**
     * @param $operaty
     * @param $shippingSourceAddress
     * @param $shippingDesAddress
     * @param $packages
     * @return string|string[]|null
     */
    private function createXmlDataPostToCreateShipment($operaty, $shippingSourceAddress, $shippingDesAddress, $packages, $shipmentItems)
    {
        //$operaty = $this->configHelper->getOperatory();
        $nroCuenta = $this->configHelper->getAccountNumber();
        $nroRemito = $shippingSourceAddress['nroremito'];

        // source shipping address
        $calleRetiro = $shippingSourceAddress['calleRetiro'];
        $nroRetiro = $shippingSourceAddress['nroRetiro'];
        $cpRetiro = $shippingSourceAddress['cpRetiro'];
        $localidadRetiro = $shippingSourceAddress['localidadRetiro'];
        $provinciaRetiro = $shippingSourceAddress['provinciaRetiro'];
        $contactoRetiro = $shippingSourceAddress['contactoRetiro'];
        $emailRetiro = $shippingSourceAddress['emailRetiro'];
        $solicitanteRetiro = $shippingSourceAddress['solicitanteRetiro'];
        $observaciones = $shippingSourceAddress['observaciones'];
        $centrocosto = $shippingSourceAddress['centrocosto'];
        $idfranjahoraria = $shippingSourceAddress['idfranjahoraria'];
        $IDCIOrigen = $shippingSourceAddress['IDCIOrigen'];
        $fecharetiro = $shippingSourceAddress['fecharetiro'];

        // destination shipping address
        $apellidoDestino = $shippingDesAddress['apellidoDestino'];
        $nombreDestino = $shippingDesAddress['nombreDestino'];
        $calleDestino = $shippingDesAddress['calleDestino'];
        $nroDestino = $shippingDesAddress['nroDestino'];
        $pisoDestino = $shippingDesAddress['pisoDestino'];
        $depto = $shippingDesAddress['depto'];
        $localidadDestino = $shippingDesAddress['localidadDestino'];
        $provinciaDestino = $shippingDesAddress['provinciaDestino'];
        $cpDestino = $shippingDesAddress['cpDestino'];
        $telefonoDestino = $shippingDesAddress['telefonoDestino'];
        $emailDestino = 'test@oca.com.ar';
        $idci = $shippingDesAddress['idci'];
        $celularDestino = $shippingDesAddress['celularDestino'];
        $observaciones = $shippingDesAddress['observaciones'];

        $xmlPackages = [];
        foreach ($shipmentItems as $item) {
            $xmlPackages[] = $this->packageToXml($item, $packages);
        }

        $xmlRequest = <<<EOF
<?xml version="1.0" encoding="ISO-8859-1"?>
<ROWS>
	<cabecera ver="2.0" nrocuenta="$nroCuenta"/>
	<origenes>
        <origen calle="$calleRetiro" nro="$nroRetiro" piso="" depto="$depto" cp="$cpRetiro" localidad="$localidadRetiro" provincia="$provinciaRetiro" contacto="$contactoRetiro" email="$emailRetiro" solicitante="$solicitanteRetiro" observaciones="$observaciones" centrocosto="$centrocosto" idfranjahoraria="$idfranjahoraria" idcentroimposicionorigen="$IDCIOrigen" fecha="$fecharetiro" >
		<envios>
			<envio idoperativa="$operaty" nroremito="$nroRemito">
                <destinatario apellido="$apellidoDestino" nombre="$nombreDestino" calle="$calleDestino" nro="$nroDestino" piso="$pisoDestino" depto="" localidad="$localidadDestino" provincia="$provinciaDestino" cp="$cpDestino" telefono="$telefonoDestino" email="$emailDestino" idci="$idci" celular="$celularDestino" observaciones="Envío a eLocker OCA" />
			    <paquetes>
EOF;
        foreach ($xmlPackages as $xmlPackage) {
            $xmlRequest .= $xmlPackage;
        }

        $xmlRequest .= <<< XMLRequest
    </paquetes></envio></envios></origen></origenes></ROWS>
XMLRequest;
        $xmlRequest = preg_replace('/>\s\s*</', '><', $xmlRequest);
        //print_r($xmlRequest); die('fadsfs');
        return $xmlRequest;
    }

    /**
     * @param ShipmentItemInterface $item
     * @param $quotePackages
     * @return string
     */
    private function packageToXml($item, $quotePackages)
    {
        $package = $this->findPackageForItem($item->getProductId(), $quotePackages);

        $heightCm = 100 * $package['alto'];
        $lengthCm = 100 * $package['largo'];
        $widthCm = 100 * $package['ancho'];
        $weightKg = round($package['peso'], 2);
        $customsValue = $package['valor'] * $item->getQty();
        $quantity = 1;

        return  "<paquete alto=\"$heightCm\" ancho=\"$widthCm\" "
            . "largo=\"$lengthCm\" peso=\"$weightKg\" valor=\"$customsValue\" "
            . "cant=\"$quantity\" />";
    }

    /**
     * @param integer $itemId
     * @param array $quotePackages
     * @return array|null
     */
    private function findPackageForItem($itemId, $quotePackages)
    {
        foreach ($quotePackages as $package) {
            if ($package['product_id'] === $itemId) {
                return $package;
            }
        }
        return null;
    }

    /**
     * @param Quote $quote
     * @return mixed
     */
    private function getQuoteOcaShippingData($quote)
    {
        return json_decode($quote->getData('oca_shipping_data'), true);
    }
    /**
     * @param Quote $quote
     * @return mixed
     */
    private function getQuoteOperaty($quote)
    {
        return $this->getQuoteOcaShippingData($quote)['operaty'];
    }

    /**
     * @param Quote $quote
     * @return mixed
     */
    private function getQuotePackages($quote)
    {
        return $this->getQuoteOcaShippingData($quote)['package'];
    }

    /**
     * @param Quote $quote
     * @return array
     */
    private function prepareDataForToBranch($quote)
    {
        $ocaShippingData = json_decode($quote->getData('oca_shipping_data'), true);
        $shippingAddress = $ocaShippingData['shipping_address'];
        return [
            'apellidoDestino' => $shippingAddress['Sigla'],
            'nombreDestino' => $shippingAddress['Sucursal'],
            'calleDestino' => $shippingAddress['Calle'],
            'nroDestino' => $shippingAddress['Numero'],
            'pisoDestino' => $shippingAddress['Piso'],
            'depto' => '',
            'localidadDestino' => $shippingAddress['Localidad'],
            'provinciaDestino' => $shippingAddress['Provincia'],
            'cpDestino' => $shippingAddress['CodigoPostal'],
            'telefonoDestino' => $shippingAddress['Telefono'],
            'emailDestino' => '',
            'idci' => $shippingAddress['IdCentroImposicion'],
            'solicitante' => '',
            'observaciones' => '',
            'celularDestino' => null
        ];
    }

    /**
     * @param Quote $quote
     * @return array
     */
    private function prepareDataForToDoor($quote)
    {
        /** @var Address $shippingAddress */
        $shippingAddress = $quote->getShippingAddress();
        $street = $shippingAddress->getStreet();
        if (is_array($street)) {
            $street = implode(",", $street);
        }
        return [
            'apellidoDestino' => $shippingAddress->getFirstname(),
            'nombreDestino' => $shippingAddress->getLastname(),
            'calleDestino' => $street,
            'nroDestino' => '', // number
            'pisoDestino' => '', // floor
            'depto' => '', // department number
            'localidadDestino' => $shippingAddress->getCity(),
            'provinciaDestino' => $shippingAddress->getRegion(),
            'cpDestino' => $shippingAddress->getPostcode(),
            'telefonoDestino' => $shippingAddress->getTelephone(),
            'emailDestino' => $shippingAddress->getEmail(),
            'idci' => 0,
            'solicitante' => '',
            'observaciones' => '',
            'celularDestino' => null
        ];
    }

    /**
     * @param Quote $quote
     * @return int
     */
    private function getShippingType($quote)
    {
        return (int)$quote->getData('oca_shipping_type');
    }

    /**
     * @param int $shipmentId
     * @return bool
     */
    public function isOcaShipment($shipmentId)
    {
        $shipment = $this->getShipment($shipmentId);
        $order = $this->orderRepository->get($shipment->getOrderId());
        if ($order->getShippingMethod() == self::OCA_SHIPPING_METHOD) {
            return true;
        }
        return false;
    }

    /**
     * @param integer $shipmentId
     * @return bool
     */
    public function isCreatedOcaShipment($shipmentId)
    {
        $shipment = $this->getShipment($shipmentId);
        if ($shipment->getData('oca_elocker_orden_retiro') && $shipment->getData('oca_elocker_numero_envio')) {
            return true;
        }
        return false;
    }
    /**
     * @param int $shipmentId
     * @return ShipmentInterface
     */
    private function getShipment($shipmentId)
    {
        return $this->shipmentRepository->get($shipmentId);
    }

    /**
     * @param int $shipmentId
     * @throws LocalizedException
     * @throws SoapFault
     */
    public function syncTrackingNumbers($shipmentId)
    {
        $listTrackingNumberSync = $this->adapter->trackingPieza($shipmentId);
        $this->addTrackingNumbers($shipmentId, $listTrackingNumberSync);
    }

    /**
     * @param int $shipmentId
     * @param array $trackingDatas
     */
    protected function addTrackingNumbers($shipmentId, $trackingDatas)
    {
        $shipment = $this->shipmentRepository->get($shipmentId);
        $currentTracks = $shipment->getTracks();
        foreach ($trackingDatas as $trackingData) {
            if ($this->isTrackingDataExists($currentTracks, $trackingData['track_number'])) {
                continue;
            }
            $track = $this->shipmentTrackInterfaceFactory->create()->setNumber(
                $trackingData['track_number']
            )->setCarrierCode(
                OcaShipping::CARRIER_CODE
            )->setTitle(
                $trackingData['description']
            );
            $shipment->addTrack($track);
        }
        $this->shipmentRepository->save($shipment);
    }

    /**
     * @param ShipmentTrackInterface[] $currentTrackingDatas
     * @param $trackingNumber
     * @return bool
     */
    private function isTrackingDataExists($currentTrackingDatas, $trackingNumber)
    {
        foreach ($currentTrackingDatas as $trackingData) {
            if ($trackingData->getTrackNumber() == $trackingNumber) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param int $shipmentId
     * @throws LocalizedException
     */
    public function cancelOcaShipment($shipmentId)
    {
        $shipment = $this->getShipment($shipmentId);
        $ordenRetiro = $shipment->getData('oca_elocker_orden_retiro');
        $this->adapter->anularOrdenGenerada($ordenRetiro);
        $shipment->setData('oca_elocker_orden_retiro', null);
        $shipment->setData('oca_elocker_numero_envio', null);
        $this->shipmentRepository->save($shipment);
    }

    /**
     * @param integer $shipmentId
     * @return array
     */
    public function getOcaShipmentInfo($shipmentId)
    {
        $shipment = $this->getShipment($shipmentId);
        return [
            'orden_retiro' => $shipment->getData('oca_elocker_orden_retiro'),
            'numero_envio' => $shipment->getData('oca_elocker_numero_envio')
        ];
    }
}
