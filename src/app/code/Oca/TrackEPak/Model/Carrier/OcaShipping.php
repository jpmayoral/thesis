<?php

namespace Oca\TrackEPak\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method as AddressRateResultMethod;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result as RateResult;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Shipping\Model\Shipment\RequestFactory;
use Magento\Shipping\Model\Tracking\Result as TrackingResult;
use Magento\Shipping\Model\Tracking\Result\Status as TrackingResultStatus;
use Oca\TrackEPak\Api\Data\OcaTrackEPakOperatoryInterface;
use Oca\TrackEPak\Helper\Config;
use Oca\TrackEPak\Model\Carrier\Flatrate\ItemPriceCalculator;
use Oca\TrackEPak\Model\OcaTrackEPakOperatory;
use Oca\TrackEPak\Service\GetShippingPrice as SoapCalculateShipping;
use Psr\Log\LoggerInterface;

/**
 * Class OcaShipping
 * @package Oca\TrackEPak\Model\Carrier
 */
class OcaShipping extends AbstractCarrier implements CarrierInterface
{
    const CARRIER_CODE = 'ocashipping';

    /**
     * @var string
     */
    protected $_code = self::CARRIER_CODE;

    /**
     * @var SoapCalculateShipping
     */
    private $soapCalculateShipping;

    /**
     * @var ResultFactory
     */
    private $rateResultFactory;

    /**
     * @var MethodFactory
     */
    private $rateMethodFactory;

    /**
     * @var ItemPriceCalculator
     */
    private $itemPriceCalculator;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * @var MessageManagerInterface
     */
    private $messageManagerInterface;

    /**
     * @var TrackingResult
     */
    private $trackingResult;

    /**
     * @var TrackingResultStatus
     */
    private $trackingResultStatus;

    /**
     * @var OcaTrackEPakOperatory
     */
    private $operatoryModel;

    /**
     * @var Config
     */
    private $configHelper;

    /**
     * OcaShipping constructor.
     * @param Config $configHelper
     * @param TrackingResultStatus $trackingResultStatus
     * @param TrackingResult $trackingResult
     * @param MessageManagerInterface $messageManagerInterface
     * @param RequestFactory $requestFactory
     * @param SoapCalculateShipping $soapCalculateShipping
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param ItemPriceCalculator $itemPriceCalculator
     * @param OcaTrackEPakOperatory $operatoryModel
     * @param array $data
     */
    public function __construct(
        Config $configHelper,
        TrackingResultStatus $trackingResultStatus,
        TrackingResult $trackingResult,
        MessageManagerInterface $messageManagerInterface,
        RequestFactory $requestFactory,
        SoapCalculateShipping $soapCalculateShipping,
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        ItemPriceCalculator $itemPriceCalculator,
        OcaTrackEPakOperatory $operatoryModel,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->soapCalculateShipping = $soapCalculateShipping;
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->itemPriceCalculator = $itemPriceCalculator;
        $this->requestFactory = $requestFactory;
        $this->messageManagerInterface = $messageManagerInterface;
        $this->trackingResult = $trackingResult;
        $this->trackingResultStatus = $trackingResultStatus;
        $this->operatoryModel = $operatoryModel;
        $this->configHelper = $configHelper;
    }

    /**
     * Custom Shipping Rates Collector
     *
     * @param RateRequest $request
     * @return RateResult|bool
     */
    public function collectRates(RateRequest $request)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/oca.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);

        if (!$this->isShowOcaShippingMethod()) {
            return false;
        }

        $isUsingFlatRateToCalculate = $this->getConfigData('is_using_flat_rate');

        if (!$isUsingFlatRateToCalculate) {
            $logger->info('OCA collect rates');
            $shippingPrice = $this->soapCalculateShipping->handleApi($request);
        } else {
            $freeBoxes = $this->getFreeBoxesCount($request);
            $this->setFreeBoxes($freeBoxes);
            $shippingPrice = $this->getShippingPrice($request, $freeBoxes);
        }

        $logger->info( 'shippingPrice: ' . $shippingPrice );

        /** @var RateResult $result */
        $result = $this->rateResultFactory->create();
        if ($shippingPrice !== false) {
            $method = $this->createResultMethod($shippingPrice);
            $result->append($method);
        }

        return $result;
    }

    /**
     * @return bool
     */
    private function isShowOcaShippingMethod()
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        /** @var OcaTrackEPakOperatoryInterface $toDoorItem */
        $toDoorItem = $this->operatoryModel->getDefaultToDoor();
        /** @var OcaTrackEPakOperatoryInterface $toBranchItem */
        $toBranchItem = $this->operatoryModel->getDefaultToBranch();

        if (!$toDoorItem->getEntityId() && !$toBranchItem->getEntityId()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * @param RateRequest $request
     * @param int $freeBoxes
     * @return bool|float
     */
    private function getShippingPrice(RateRequest $request, $freeBoxes)
    {
        $shippingPrice = false;

        $configPrice = $this->getConfigData('shipping_cost');

        if ($this->getConfigData('type') === 'O') {
            // per order
            $shippingPrice = $this->itemPriceCalculator->getShippingPricePerOrder($request, $configPrice, $freeBoxes);
        } elseif ($this->getConfigData('type') === 'I') {
            // per item
            $shippingPrice = $this->itemPriceCalculator->getShippingPricePerItem($request, $configPrice, $freeBoxes);
        }

        $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);

        if ($shippingPrice !== false && $request->getPackageQty() == $freeBoxes) {
            $shippingPrice = '0.00';
        }
        return $shippingPrice;
    }

    /**
     * @param RateRequest $request
     * @return int
     */
    private function getFreeBoxesCount(RateRequest $request)
    {
        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    $freeBoxes += $this->getFreeBoxesCountFromChildren($item);
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        return $freeBoxes;
    }

    /**
     * @param mixed $item
     * @return mixed
     */
    private function getFreeBoxesCountFromChildren($item)
    {
        $freeBoxes = 0;
        foreach ($item->getChildren() as $child) {
            if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                $freeBoxes += $item->getQty() * $child->getQty();
            }
        }
        return $freeBoxes;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [self::CARRIER_CODE => $this->getConfigData('name')];
    }

    /**
     * @param int|float $shippingPrice
     * @return AddressRateResultMethod
     */
    private function createResultMethod($shippingPrice)
    {
        /** @var AddressRateResultMethod $method */
        $method = $this->rateMethodFactory->create();

        $method->setCarrier(self::CARRIER_CODE);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod(self::CARRIER_CODE);
        $method->setMethodTitle($this->getConfigData('name'));

        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);
        return $method;
    }

    /**
     * @return bool
     */
    public function isShippingLabelsAvailable()
    {
        return false;
    }

    /**
     * @param $trackings
     * @return TrackingResult
     */
    public function getTracking($trackings)
    {
        if (!is_array($trackings)) {
            $trackings = [$trackings];
        }

        foreach ($trackings as $tracking) {
            $this->trackingResultStatus->setCarrier($this->getConfigData('code'));
            $this->trackingResultStatus->setCarrierTitle($this->getConfigData('title'));
            $this->trackingResultStatus->setTracking($tracking);
            $this->trackingResultStatus->setPopup(1);
            $this->trackingResultStatus->setUrl($this->configHelper->getTrackingUrl() . $tracking);
            $this->trackingResult->append($this->trackingResultStatus);
        }

        return $this->trackingResult;
    }

    /**
     * @param $tracking
     * @return bool|TrackingResult
     */
    public function getTrackingInfo($tracking)
    {
        $result = $this->getTracking($tracking);

        if ($result instanceof TrackingResult) {
            if ($trackings = $result->getAllTrackings()) {
                return $trackings[0];
            }
        } elseif (is_string($result) && !empty($result)) {
            return $result;
        }

        return false;
    }
}
