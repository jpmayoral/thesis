<?php

namespace Oca\TrackEPak\Model\Carrier\Flatrate;

use Magento\Quote\Model\Quote\Address\RateRequest;

/**
 * Class ItemPriceCalculator
 * @package Oca\TrackEPak\Model\Carrier\Flatrate
 */
class ItemPriceCalculator
{
    /**
     * @param RateRequest $request
     * @param int $basePrice
     * @param int $freeBoxes
     * @return float
     */
    public function getShippingPricePerItem(
        RateRequest $request,
        $basePrice,
        $freeBoxes
    ) {
        return $request->getPackageQty() * $basePrice - $freeBoxes * $basePrice;
    }

    /**
     * @param RateRequest $request
     * @param int $basePrice
     * @param int $freeBoxes
     * @return float
     */
    public function getShippingPricePerOrder(
        RateRequest $request,
        $basePrice,
        $freeBoxes
    ) {
        return $basePrice;
    }
}
