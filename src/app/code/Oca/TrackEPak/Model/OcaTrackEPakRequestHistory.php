<?php

namespace Oca\TrackEPak\Model;

use Magento\Framework\Model\AbstractModel;
use Oca\TrackEPak\Api\Data\OcaTrackEPakRequestHistoryInterface;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory as OcaTrackEPakRequestHistoryResourceModel;

/**
 * Class OcaTrackEPakRequestHistory
 * @package Oca\TrackEPak\Model
 */
class OcaTrackEPakRequestHistory extends AbstractModel implements OcaTrackEPakRequestHistoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * {@inheritDoc}
     */
    public function getRequestLink()
    {
        return $this->getData(self::REQUEST_LINK);
    }

    /**
     * {@inheritDoc}
     */
    public function setRequestLink($requestLink)
    {
        return $this->setData(self::REQUEST_LINK, $requestLink);
    }

    /**
     * {@inheritDoc}
     */
    public function getRequestData()
    {
        return $this->getData(self::REQUEST_DATA);
    }

    /**
     * {@inheritDoc}
     */
    public function setRequestData($requestData)
    {
        return $this->setData(self::REQUEST_DATA, $requestData);
    }

    /**
     * {@inheritDoc}
     */
    public function getResponseData()
    {
        return $this->getData(self::RESPONSE_DATA);
    }

    /**
     * {@inheritDoc}
     */
    public function setResponseData($responseData)
    {
        return $this->setData(self::RESPONSE_DATA, $responseData);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Construct Function
     */
    protected function _construct()
    {
        $this->_init(OcaTrackEPakRequestHistoryResourceModel::class);
    }
}
