<?php

namespace Oca\TrackEPak\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Oca\TrackEPak\Api\Data\OcaTrackEPakRequestHistoryInterface;
use Oca\TrackEPak\Api\OcaTrackEPakRequestHistoryRepositoryInterface;
use Oca\TrackEPak\Model\ResourceModel\OcaTrackEPakRequestHistory as OcaTrackEPakResourceModel;

/**
 * Class OcaTrackEPakRepository
 * @package Oca\TrackEPak\Model
 */
class OcaTrackEPakRequestHistoryRepository implements OcaTrackEPakRequestHistoryRepositoryInterface
{
    /**
     * @var OcaTrackEPakRequestHistoryInterface
     */
    protected $requestHistoryInterfaceFactory;

    /**
     * @var OcaTrackEPakResourceModel
     */
    protected $requestHistoryResourceModel;

    /**
     * OcaTrackEPakRepository constructor.
     * @param OcaTrackEPakRequestHistoryInterface $requestHistoryInterfaceFactory
     * @param OcaTrackEPakResourceModel $requestHistoryResourceModel
     */
    public function __construct(
        OcaTrackEPakRequestHistoryInterface $requestHistoryInterfaceFactory,
        OcaTrackEPakResourceModel $requestHistoryResourceModel
    ) {
        $this->requestHistoryInterfaceFactory = $requestHistoryInterfaceFactory;
        $this->requestHistoryResourceModel = $requestHistoryResourceModel;
    }

    /**
     * @inheritdoc
     */
    public function save(OcaTrackEPakRequestHistoryInterface $requestHistory)
    {
        try {
            $this->requestHistoryResourceModel->save($requestHistory);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $requestHistory;
    }

    /**
     * @inheritdoc
     */
    public function get($requestHistoryId)
    {
        /** @var OcaTrackEPakRequestHistoryInterface $requestHistoryModel */
        $requestHistoryModel = $this->requestHistoryInterfaceFactory->create();
        $this->requestHistoryResourceModel->load($requestHistoryModel, $requestHistoryId);
        if (!$requestHistoryModel->getId()) {
            throw new NoSuchEntityException(__('Request history object with id "%1" does not exist.', $requestHistoryId));
        }

        return $requestHistoryModel;
    }

    /**
     * @inheritdoc
     */
    public function delete($ocaTrackEPakRequestHistoryId)
    {
        $this->requestHistoryResourceModel->deleteById($ocaTrackEPakRequestHistoryId);
    }
}
