;define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/quote',
], function ($, Component, ko, quote) {
    'use strict';

    var ocaShippingMethod = window.checkoutConfig.ocaShippingMethod;
    var isToMyDoor = window.checkoutConfig.isToMyDoor;
    var isToBranch = window.checkoutConfig.isToBranch;
    var getBranchUrl = window.checkoutConfig.getBranchUrl;
    var checkedDefault = window.checkoutConfig.checkedDefault;
    var selectedBranchArray = window.checkoutConfig.selectedBranchArray;
    var selectedValueBranchList = window.checkoutConfig.selectedValueBranchList;
    var branchList = window.checkoutConfig.branchList;
    var shipToSpecificCountries = window.checkoutConfig.shipToSpecificCountries;
    var defaultCountry = window.checkoutConfig.defaultCountry;
    var branchInfoKeys = window.checkoutConfig.branchInfoKeys;

    return Component.extend({
        visible: ko.observable(!quote.isVirtual()),
        defaults: {
            template: 'Oca_TrackEPak/additional-oca-shipping'
        },
        formData: {
            date: 'abc',
        },
        selectedToBranch: (checkedDefault == 'to_branch'),
        showWarningMessage: (!selectedBranchArray),
        warningMessage: ko.observable('Please search branch and pick one'),
        isToMyDoor: isToMyDoor,
        isToBranch: isToBranch,
        checkedDefault: checkedDefault,
        selectedValueBranchList: ko.observable(selectedValueBranchList),
        branchList: ko.observableArray(branchList),

        initialize: function () {
            var self = this;
            this._super();
        },

        initObservable: function () {
            this._super();
            this.selectedMethod = ko.computed(function () {
                console.log('selectedMethod');
                let method = quote.shippingMethod() ?
                    quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                    null;
                let isOcaShipping = (method == ocaShippingMethod);
                this.handleShippingAddressForm(isOcaShipping);
                return isOcaShipping;
            }, this);

            return this;
        },

        myAfterRender: function () {
            console.log('function myAfterRender');
            this.afterLoad();
        },

        afterLoad: function () {
            console.log('function afterLoad');
            this.setSelectedBranch(selectedBranchArray);
        },

        showHideShippingAddressForm: function (show) {
            if (show) {
                $('#co-shipping-form').show();
            } else {
                $('#co-shipping-form').hide();
            }
        },

        handleShippingAddressForm: function (isOcaShipping) {
            console.log('function handleShippingAddressForm');
            this.setupAddressCountry(isOcaShipping);
            let show = !(isOcaShipping && this.checkedDefault != 'to_my_door');
            this.showHideShippingAddressForm(show);
        },

        setupAddressCountry: function(isOcaShipping){
            if (shipToSpecificCountries === false) {
                return;
            }

            if (isOcaShipping) {
                $("#shipping-new-address-form select[name='country_id'] option").attr('disabled', true).addClass('no-display');
                $('#shipping-new-address-form select[name="country_id"] option[value=""]').attr('disabled', false).removeClass('no-display');
                $.each(shipToSpecificCountries, function (index, value) {
                    $('#shipping-new-address-form select[name="country_id"] option[value="' + value + '"]').attr('disabled', false).removeClass('no-display');
                });

                let selectedCountry = $("#shipping-new-address-form select[name='country_id']").val();
                if (!selectedCountry || !$.inArray(selectedCountry, shipToSpecificCountries)) {
                    $("#shipping-new-address-form select[name='country_id']").val(defaultCountry).trigger('change');
                }
            } else {
                $("#shipping-new-address-form select[name='country_id'] option").attr('disabled', false).removeClass('no-display');
            }
        },

        handleSearchBranch: function (showSearch) {
            if (showSearch) {
                $('#search_branch').show();
            } else {
                $('#search_branch').hide();
            }
        },

        searchBranches: function() {
            let ocazipcode = $("#oca_zipcode").val();
            let oldZipcode = $("#oca_zipcode").attr('zipcode');
            if (oldZipcode != ocazipcode && ocazipcode.length >= 4) {
                $.ajax({
                    showLoader: true,
                    url: getBranchUrl,
                    data: {'zipcode':ocazipcode},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        let html = '<option>'+$.mage.__('Choose a Branch...')+'</option>';
                        let i = 0;
                        let hasSelected = false;
                        if (data) {
                            for (i; i < data.length; i++) {
                                let selected = '';
                                if (selectedValueBranchList) {
                                    let mybranch = JSON.parse(selectedValueBranchList);
                                    console.log(mybranch['Sucursal'] , data[i]['Sucursal']);
                                    if (mybranch['Sucursal'] == data[i]['Sucursal']
                                        && mybranch['CodigoPostal'] == data[i]['CodigoPostal']
                                    ) {
                                        console.log('===');
                                        selected = 'selected="selected"';
                                        hasSelected = true;
                                    }
                                }

                                html = html + '<option '+selected+' value=\'' + JSON.stringify(data[i]) + '\'>' + data[i]['Sucursal'] + ' (' + data[i]['CodigoPostal'] + ')</option>';
                            }
                        }
                        $("#branch_list").html(html);
                        $("#oca_zipcode").attr('zipcode', ocazipcode);
                        if (!hasSelected) {
                            $('#previewSelectedBranch').html('');
                            $('#validate-branch-message').show();
                        } else {
                            $('#validate-branch-message').hide();
                        }
                    }
                });
            }
        },

        operatoryMethodChange: function (data, event) {
            console.log('function operatoryMethodChange');
            let selectedOperatoryMethod = event.target.id;
            this.showHideShippingAddressForm(selectedOperatoryMethod == 'input_to_my_door');
            let showSearch = $('#input_to_branch:checked').length > 0;
            this.handleSearchBranch(showSearch);
            this.setSelectedBranch(selectedBranchArray);
        },

        setSelectedBranch: function(branchData) {
            console.log('setSelectedBranch', branchData);
            if (branchData) {
                this.branchChange();

                let html = '';
                $.each(branchData, function( index, value ) {
                    if (branchInfoKeys[index] !== undefined) {
                        html = html + '<div><strong>'+branchInfoKeys[index]+'</strong>: '+value+'</div>';
                    }
                });

                $('#validate-branch-message').hide();
                $('#previewSelectedBranch').html(html);
            } else {
                $('#validate-branch-message').show();
            }
        },

        branchChange: function () {
            let branchData = $("#branch_list").val();
            console.log('branchChange', branchData);
            $('#previewSelectedBranch').html('');
            if (branchData) {
                branchData = JSON.parse(branchData);
                $("#shipping-new-address-form input[name='firstname']").val(branchData['Sucursal']);
                $("#shipping-new-address-form input[name='firstname']").trigger('change');

                $("#shipping-new-address-form input[name='lastname']").val(branchData['Sigla']);
                $("#shipping-new-address-form input[name='lastname']").trigger('change');

                $("#shipping-new-address-form input[name='street[0]']").val(branchData['Numero']+' '+branchData['Calle']);
                $("#shipping-new-address-form input[name='street[0]']").trigger('change');

                $("#shipping-new-address-form input[name='city']").val(branchData['Provincia']);
                $("#shipping-new-address-form input[name='city']").trigger('change');

                $("#shipping-new-address-form select[name='country_id']").val(defaultCountry);
                $("#shipping-new-address-form select[name='country_id']").trigger('change');
                
                $.each($("#shipping-new-address-form select[name='region_id'] > option"), function () {
                    if ($(this).val()) {
                        $("#shipping-new-address-form select[name='region_id']").val($(this).val());
                        return false;
                    }
                });

                $("#shipping-new-address-form input[name='telephone']").val(branchData['Telefono']);
                $("#shipping-new-address-form input[name='telephone']").trigger('change');

                $("#shipping-new-address-form input[name='postcode']").val(branchData['CodigoPostal']);
                $("#shipping-new-address-form input[name='postcode']").trigger('change');

                let html = '';
                $.each(branchData, function( index, value ) {
                    if (branchInfoKeys[index] !== undefined) {
                        html = html + '<div><strong>'+branchInfoKeys[index]+'</strong>: '+value+'</div>';
                    }
                });
                $('#validate-branch-message').hide();
                $('#previewSelectedBranch').html(html);
            } else {
                $('#validate-branch-message').show();
            }
        }

    });
});