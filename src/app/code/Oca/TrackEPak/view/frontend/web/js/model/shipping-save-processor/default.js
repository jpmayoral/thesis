define([
    'jquery',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/model/payment/method-converter',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/model/shipping-save-processor/payload-extender'
], function (
    $,
    ko,
    quote,
    resourceUrlManager,
    storage,
    paymentService,
    methodConverter,
    errorProcessor,
    fullScreenLoader,
    selectBillingAddressAction,
    payloadExtender
) {
    'use strict';

    var operativeToDoor = window.checkoutConfig.operativeToDoor;
    var operativeToBranch = window.checkoutConfig.operativeToBranch;

    return {
        /**
         * @return {jQuery.Deferred}
         */
        saveShippingInformation: function () {
            var payload;
            let shippingMethodCode = quote.shippingMethod().method_code;
            let oca_shipping_type = null;
            let oca_shipping_data = null;

            if (
                !quote.billingAddress() &&
                (quote.shippingAddress().canUseForBilling() || shippingMethodCode == 'ocashipping')
            ) {
                selectBillingAddressAction(quote.shippingAddress());
            }

            if (shippingMethodCode == 'ocashipping') {
                oca_shipping_data = {};

                if ($('#input_to_my_door:checked').length) {
                    oca_shipping_type = 1;
                    oca_shipping_data['operaty'] = operativeToDoor;
                } else {
                    oca_shipping_type = 2;
                    oca_shipping_data['operaty'] = operativeToBranch;
                    oca_shipping_data['shipping_address'] = JSON.parse($("#branch_list").val());
                }

                oca_shipping_data = JSON.stringify(oca_shipping_data);
            }

            payload = {
                addressInformation: {
                    shipping_address: quote.shippingAddress(),
                    billing_address: quote.billingAddress(),
                    shipping_method_code: quote.shippingMethod().method_code,
                    shipping_carrier_code: quote.shippingMethod().carrier_code
                }
            };

            payloadExtender(payload);

            //Set extension_attributes to payload after paloadExtender called
            let addressExtensionAttribute = payload.addressInformation.extension_attributes;
            if (addressExtensionAttribute == undefined) {
                addressExtensionAttribute = {};
            }
            addressExtensionAttribute.oca_shipping_type = oca_shipping_type;
            addressExtensionAttribute.oca_shipping_data = oca_shipping_data;
            payload.addressInformation.extension_attributes = addressExtensionAttribute;
            console.log(payload);

            fullScreenLoader.startLoader();

            return storage.post(
                resourceUrlManager.getUrlForSetShippingInformation(quote),
                JSON.stringify(payload)
            ).done(
                function (response) {
                    quote.setTotals(response.totals);
                    paymentService.setPaymentMethods(methodConverter(response['payment_methods']));
                    fullScreenLoader.stopLoader();
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                    fullScreenLoader.stopLoader();
                }
            );
        }
    };
});
