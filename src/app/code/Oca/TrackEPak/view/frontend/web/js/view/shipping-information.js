/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/sidebar',
    'ko',
], function ($, Component, quote, stepNavigator, sidebarModel, ko) {
    'use strict';

    var ocaShippingMethod = window.checkoutConfig.ocaShippingMethod;
    var selectedBranch = window.checkoutConfig.selectedBranch;
    var selectedBranchArray = window.checkoutConfig.selectedBranchArray;
    var branchInfoKeys = window.checkoutConfig.branchInfoKeys;

    return Component.extend({
        defaults: {
            template: 'Oca_TrackEPak/shipping-information'
        },

        initObservable: function () {
            this._super();

            this.selectedMethod = ko.computed(function () {
                console.log('Triiger here');
                let method = quote.shippingMethod() ?
                    quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                    null;
                let isToBranchSelected = $('#input_to_branch:checked').length;
                let isOcaShippingToBranch = (method == ocaShippingMethod) && isToBranchSelected;
                console.log('isOcaShippingToBranch', isOcaShippingToBranch);
                let previewData = $("#previewSelectedBranch").html();
                console.log('previewData', previewData);
                if (isOcaShippingToBranch && previewData) {
                    $('#ship-to-branch-info .branchinfo').html(previewData);
                }
                if (isOcaShippingToBranch && selectedBranchArray) {
                    this.setSelectedBranch(selectedBranchArray);
                }

                return isOcaShippingToBranch;
            }, this);

            return this;
        },

        getSelectedBranchInfo: function () {
            console.log('getSelectedBranchInfo', selectedBranch);
            let previewData = $("#previewSelectedBranch").html();
            if (!selectedBranch && previewData) {
                return previewData;
            }
            return selectedBranch;
        },

        setSelectedBranch: function(branchData) {
            console.log('si setSelectedBranch', branchData);
            if (branchData) {
                this.branchChange();

                let html = '';
                $.each(branchData, function( index, value ) {
                    if (branchInfoKeys[index] !== undefined) {
                        html = html + '<div><strong>'+branchInfoKeys[index]+'</strong>: '+value+'</div>';
                    }
                });

                $('#validate-branch-message').hide();
                $('#previewSelectedBranch').html(html);
            } else {
                $('#validate-branch-message').show();
            }
        },

        branchChange: function () {
            let branchData = $("#branch_list").val();
            console.log('si branchChange', branchData);
            $('#previewSelectedBranch').html('');
            if (branchData) {
                branchData = JSON.parse(branchData);
                $("#shipping-new-address-form input[name='firstname']").val(branchData['Sucursal']);
                $("#shipping-new-address-form input[name='firstname']").trigger('change');

                $("#shipping-new-address-form input[name='lastname']").val(branchData['Sigla']);
                $("#shipping-new-address-form input[name='lastname']").trigger('change');

                $("#shipping-new-address-form input[name='street[0]']").val(branchData['Numero']+' '+branchData['Calle']);
                $("#shipping-new-address-form input[name='street[0]']").trigger('change');

                $("#shipping-new-address-form input[name='city']").val(branchData['Provincia']);
                $("#shipping-new-address-form input[name='city']").trigger('change');

                $("#shipping-new-address-form select[name='country_id']").val('AF');
                $("#shipping-new-address-form select[name='country_id']").trigger('change');

                $("#shipping-new-address-form input[name='telephone']").val(branchData['Telefono']);
                $("#shipping-new-address-form input[name='telephone']").trigger('change');

                $("#shipping-new-address-form input[name='postcode']").val(branchData['CodigoPostal']);
                $("#shipping-new-address-form input[name='postcode']").trigger('change');

                let html = '';
                $.each(branchData, function( index, value ) {
                    if (branchInfoKeys[index] !== undefined) {
                        html = html + '<div><strong>'+branchInfoKeys[index]+'</strong>: '+value+'</div>';
                    }
                });
                $('#validate-branch-message').hide();
                $('#previewSelectedBranch').html(html);
            } else {
                $('#validate-branch-message').show();
            }
        },

        /**
         * @return {Boolean}
         */
        isVisible: function () {
            return !quote.isVirtual() && stepNavigator.isProcessed('shipping');
        },

        /**
         * @return {String}
         */
        getShippingMethodTitle: function () {
            var shippingMethod = quote.shippingMethod();

            return shippingMethod ? shippingMethod['carrier_title'] + ' - ' + shippingMethod['method_title'] : '';
        },

        /**
         * Back step.
         */
        back: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping');
        },

        /**
         * Back to shipping method.
         */
        backToShippingMethod: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping', 'opc-shipping_method');
        }
    });
});
