<?php

namespace Oca\TrackEPak\Service;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Item;

/**
 * Class Soap
 * @package Oca\TrackEPak\Service
 */
class GetShippingPrice extends AbstractSoap
{
    protected $params = [];
    const LBS_TO_KGS_RATE = 0.45;
    const INCH_TO_METRE_RATE = 0.0254;
    const DEFAULT_WEIGHT = 10; //kgs
    const VOLUME_DIVISION = 1000000;
    const DEFAULT_VOLUME = 0.001;
    const NUMBER_OF_PACKAGE = 1;

    /**
     * @inheritdoc
     */
    public function handleApi($inputData = null)
    {
        $this->prepareParams($inputData);
        return ($this->sendRequest());
    }

    /**
     * @param RateRequest|null $request
     * @return array|mixed
     */
    protected function prepareParams($request)
    {
        $defaultHeight = $this->configHelper->getProductDimensionHeight();
        $defaultWidth = $this->configHelper->getProductDimensionWidth();
        $defaultLength = $this->configHelper->getProductDimensionLength();
        $weightUnit = $this->configHelper->getWeightUnit();
        $cuit = $this->configHelper->getCuit();
        $warehouse = $this->configHelper->getWarehouseInformation();
        $zipcode = $warehouse['zipcode']?? '';
        $weightRate = self::LBS_TO_KGS_RATE;
        if ($weightUnit == 'kgs') {
            $weightRate = 1;
        }
        $orderTotalWeight = 0;
        $totalVolume = 0;
        $allItem = $request->getAllItems();
        $subtotal = 0;
        /** @var Item $item */
        foreach ($allItem as $item) {
            if (!$item->getIsVirtual()) {
                $orderTotalWeight += $item->getRowWeight() * $weightRate;
                $subtotal += $item->getRowTotal();

                $dimensionHeight = $item->getProduct()->getData('ts_dimensions_height') * self::INCH_TO_METRE_RATE;
                $dimensionWidth = $item->getProduct()->getData('ts_dimensions_width') * self::INCH_TO_METRE_RATE;
                $dimensionLength = $item->getProduct()->getData('ts_dimensions_length') * self::INCH_TO_METRE_RATE;
                //check default value
                $dimensionHeight = ($dimensionHeight == 0) ? $defaultHeight : $dimensionHeight;
                $dimensionWidth = ($dimensionWidth == 0) ? $defaultWidth : $dimensionWidth;
                $dimensionLength = ($dimensionLength == 0) ? $defaultLength : $dimensionLength;

                $volume = ($dimensionHeight * $dimensionWidth * $dimensionLength)/self::VOLUME_DIVISION;
                $totalVolume += $item->getQty() * $volume;
            }
        }

        $orderTotalWeight = ($orderTotalWeight > self::DEFAULT_WEIGHT) ? $orderTotalWeight : self::DEFAULT_WEIGHT;
        $totalVolume = ($totalVolume > self::DEFAULT_VOLUME) ? $totalVolume : self::DEFAULT_VOLUME;

        //Param list:
        // PesoTotal: Total shipping weight expressed in kilograms
        // VolumenTotal: Total shipping volume expressed in cubic meters
        // CodigoPostalOrigen: Postal Code Source
        // CodigoPostalDestino: Postal Code Des
        // CantidadPaquetes: Number of Packages that make up the shipment
        // ValorDeclarado: Money value of the shipment
        // CUIT
        // Operativa: No. of Shipping Operation (64665 - door to door, 62342 - door to branch, 94584 - branch to door, 78254 - branch to branch)
        $this->params =  [
            'PesoTotal' => $orderTotalWeight,
            'VolumenTotal' => $totalVolume,
            'CodigoPostalOrigen' => $zipcode,
            'CodigoPostalDestino' => $request->getDestPostcode(),
            'CantidadPaquetes' => self::NUMBER_OF_PACKAGE,
            'ValorDeclarado' => $subtotal,
            'Cuit' => $cuit,
            'Operativa' => '64665'
        ];

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/oca.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('params', $this->params);
    }

    /**
     * @inheritdoc
     */
    protected function sendRequest()
    {
        $client = $this->createNewSoapELockerClient();
        try {
            $total = 0;
            $requestLink = $client->getWSDL() . '&&op=Tarifar_Envio_Corporativo';
            $this->ocaTrackEPakRequestHistory
                ->setRequestLink($requestLink)
                ->setStatus('success');
            $result = $client->call('Tarifar_Envio_Corporativo', [$this->params]);
            $result = simplexml_load_string($result->Tarifar_Envio_CorporativoResult->any);
            foreach ($result->children() as $secondGen) {
                foreach ($secondGen->children() as $thirdGen) {
                    $total = $thirdGen->Total;
                }
            }
        } catch (\SoapFault $exception) {
            $this->ocaTrackEPakRequestHistory->setStatus('fail');
        }

        $this->ocaTrackEPakRequestHistory
            ->setRequestData($client->getLastRequest())
            ->setResponseData($client->getLastResponse());

        $this->ocaTrackEPakRequestHistoryRepository->save($this->ocaTrackEPakRequestHistory);

        return $total;
    }

    /**
     *
     */
    protected function saveRequestApi()
    {
    }

    /**
     * @inheritdoc
     */
    protected function parseResponse($response)
    {
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    protected function writeLogRequest($request, $response)
    {
    }
}
