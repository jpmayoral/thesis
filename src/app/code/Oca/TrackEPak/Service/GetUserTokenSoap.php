<?php

namespace Oca\TrackEPak\Service;

/**
 * Class Soap
 * @package Oca\TrackEPak\Service
 */
class GetUserTokenSoap extends AbstractSoap
{
    /**
     * @inheritdoc
     */
    public function handleApi($data = null)
    {
        $result = $this->sendRequest();
    }

    /**
     * @inheritdoc
     */
    protected function prepareParams($data = null)
    {
        $userInfo = $this->configHelper->getOcaTrackEPakUserInfo();
        return [
            self::OCA_USER_FIELD => $userInfo['username'],
            self::OCA_PASSWORD_FIELD => $userInfo['password']
        ];
    }

    /**
     * @inheritdoc
     */
    protected function sendRequest()
    {
        $client = $this->createNewSoapClient();
        try {
            $client->getLoginData($this->prepareParams());
            $requestData = [
                'status' => 'success',
                'response_data' => $client->getLoginData($this->prepareParams())
            ];
            var_dump($requestData);
            die;
        } catch (\SoapFault $exception) {
            $requestData = [
                'status' => 'fail',
                'errorMessage' => $exception->getMessage()
            ];
        }
        $requestData['request_header'] = $client->getLastRequestHeaders();
        $requestData['request'] = $client->getLastRequest();
        $requestData['response_header'] = $client->getLastResponseHeaders();
        return $requestData;
    }

    /**
     * @inheritdoc
     */
    protected function parseResponse($response)
    {
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    protected function writeLogRequest($request, $response)
    {
    }
}
