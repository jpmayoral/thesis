<?php

namespace Oca\TrackEPak\Service;

use Oca\TrackEPak\Helper\Config as ConfigHelper;
use Oca\TrackEPak\Model\OcaTrackEPakRequestHistory;
use Oca\TrackEPak\Model\OcaTrackEPakRequestHistoryRepository;
use Zend\Soap\Client;
use Zend\Soap\ClientFactory;

/**
 * Class Soap
 * @package Oca\TrackEPak\Service
 */
abstract class AbstractSoap
{
    const OCA_USER_FIELD = 'usr';

    const OCA_PASSWORD_FIELD = 'psw';

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * @var ClientFactory
     */
    protected $soapClientFactory;

    protected $baseWsdlLink;

    /**
     * @var OcaTrackEPakRequestHistory
     */
    protected $ocaTrackEPakRequestHistory;

    /**
     * @var OcaTrackEPakRequestHistoryRepository
     */
    protected $ocaTrackEPakRequestHistoryRepository;

    /**
     * AbstractSoap constructor.
     * @param OcaTrackEPakRequestHistory $ocaTrackEPakRequestHistory
     * @param OcaTrackEPakRequestHistoryRepository $ocaTrackEPakRequestHistoryRepository
     * @param ConfigHelper $config
     * @param ClientFactory $clientFactory
     */
    public function __construct(
        OcaTrackEPakRequestHistory $ocaTrackEPakRequestHistory,
        OcaTrackEPakRequestHistoryRepository $ocaTrackEPakRequestHistoryRepository,
        ConfigHelper $config,
        ClientFactory $clientFactory
    ) {
        $this->configHelper = $config;
        $this->soapClientFactory = $clientFactory;
        $this->ocaTrackEPakRequestHistory = $ocaTrackEPakRequestHistory;
        $this->ocaTrackEPakRequestHistoryRepository = $ocaTrackEPakRequestHistoryRepository;
    }

    /**
     * @return Client
     */
    public function createNewSoapClient()
    {
        $this->baseWsdlLink = $this->configHelper->getOcaSoapUrl();
        return $this->soapClientFactory
                    ->create(['wsdl' => $this->configHelper->getOcaSoapUrl()]);
    }

    /**
     * @return Client
     */
    public function createNewSoapELockerClient()
    {
        $this->baseWsdlLink = $this->configHelper->getOcaElockerSoapUrl();
        return $this->soapClientFactory->create(['wsdl' => $this->baseWsdlLink]);
    }

    /**
     * @param array $inputData
     * @return mixed
     */
    abstract protected function handleApi($inputData = null);

    /**
     * @param $request
     * @return mixed
     */
    abstract protected function prepareParams($request);

    /**
     * @return mixed
     */
    abstract protected function sendRequest();

    /**
     * @param $response
     * @return mixed
     */
    abstract protected function parseResponse($response);

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    abstract protected function writeLogRequest($request, $response);
}
