<?php
namespace Oca\TrackEPak\Block\Adminhtml\Detail;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\LocalizedException;
use Oca\TrackEPak\Model\Shipping\Oca as OcaShippingModel;

/**
 * Class ExtraFields
 * @package Oca\TrackEPak\Block\Adminhtml\Detail
 */
class ExtraFields extends Template
{
    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var OcaShippingModel
     */
    protected $ocaShippingModel;

    /**
     * ExtraFields constructor.
     * @param OcaShippingModel $ocaShippingModel
     * @param FormKey $formKey
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        OcaShippingModel $ocaShippingModel,
        FormKey $formKey,
        Context $context,
        array $data = []
    ) {
        $this->ocaShippingModel = $ocaShippingModel;
        $this->formKey = $formKey;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }

    /**
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl(
            'oca_track_epak/shipment/save',
            ['_current' => true]
        );
    }

    /**
     * @return string
     */
    public function getCentrosImposicionConServiciosByCPUrl()
    {
        return $this->getUrl(
            'oca_track_epak/shipment/getCentrosImposicionConServiciosByCP',
            ['_current' => true]
        );
    }

    /**
     * @return bool
     */
    public function isOcaShipment()
    {
        return $this->ocaShippingModel->isOcaShipment($this->getShipmentId());
    }

    /**
     * @return bool
     */
    public function isCreatedShipment()
    {
        return $this->ocaShippingModel->isCreatedOcaShipment($this->getShipmentId());
    }

    /**
     * @return array
     */
    public function getOcaShipmentInfo()
    {
        return $this->ocaShippingModel->getOcaShipmentInfo($this->getShipmentId());
    }

    /**
     * @return mixed
     */
    private function getShipmentId()
    {
        return $this->getRequest()->getParam('shipment_id');
    }
}
