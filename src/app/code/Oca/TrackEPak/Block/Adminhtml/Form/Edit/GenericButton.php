<?php

namespace Oca\TrackEPak\Block\Adminhtml\Form\Edit;

use Magento\Backend\Block\Widget\Context;

/**
 * Class GenericButton
 * @package Oca\TrackEPak\Block\Adminhtml\Form\Edit
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * GenericButton constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->context = $context;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->context->getRequest()->getParam('entity_id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
