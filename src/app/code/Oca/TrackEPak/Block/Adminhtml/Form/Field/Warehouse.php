<?php

namespace Oca\TrackEPak\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class Warehouse
 * @package Oca\TrackEPak\Block\Adminhtml\Form\Field
 */
class Warehouse extends AbstractFieldArray
{
    /**
     * Render
     */
    protected function _prepareToRender()
    {
        $this->addColumn('key', ['label' => __('Key')]);
        $this->addColumn('value', ['label' => __('Value')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add field');
    }
}
