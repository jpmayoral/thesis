<?php
namespace Oca\TrackEPak\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Oca\TrackEPak\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const OCA_ELOCKER_ORDEN_RETIRO = 'oca_elocker_orden_retiro';

    const OCA_ELOCKER_NUMERO_ENVIO = 'oca_elocker_numero_envio';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $this->UpdateForVersionOneOneZero($setup);
        }
        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $this->UpdateForVersionOneTwoZero($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    protected function UpdateForVersionOneOneZero(SchemaSetupInterface $setup)
    {
        $tableName = InstallSchema::TABLE_NAME;

        $table = $setup->getTable($tableName);

        if (
            $setup->getConnection()->tableColumnExists($table, 'status') &&
            $setup->getConnection()->tableColumnExists($table, 'request_link')
        ) {
            $setup->getConnection()->addIndex(
                $setup->getTable($table),
                $setup->getConnection()->getIndexName(
                    $table,
                    [
                        'status',
                        'request_link'
                    ],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                [
                    'status',
                    'request_link'
                ],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    protected function UpdateForVersionOneTwoZero(SchemaSetupInterface $setup)
    {
        $table = $setup->getTable('sales_shipment');

        if (
            !$setup->getConnection()->tableColumnExists($table, self::OCA_ELOCKER_ORDEN_RETIRO) &&
            !$setup->getConnection()->tableColumnExists($table, self::OCA_ELOCKER_NUMERO_ENVIO)
        ) {
            $setup->getConnection()->addColumn(
                $table,
                self::OCA_ELOCKER_ORDEN_RETIRO,
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'OCA ELOCKER ORDEN RETIRO',
                ]
            );

            $setup->getConnection()->addColumn(
                $table,
                self::OCA_ELOCKER_NUMERO_ENVIO,
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'OCA ELOCKER NUMERO ENVIO',
                ]
            );
        }
    }
}
