<?php

namespace Oca\TrackEPak\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Oca\TrackEPak\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    const TABLE_NAME = 'oca_track_epak_request_history';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'oca_track_epak_request_history'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable(self::TABLE_NAME)
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
                'unsigned' => true,
                'identity' => true,
                'primary' => true
            ],
            'Entity Id'
        )->addColumn(
            'status',
            Table::TYPE_TEXT,
            20,
            [
                'nullable' => false,
            ],
            'Request status'
        )->addColumn(
            'request_link',
            Table::TYPE_TEXT,
            2048,
            [
                'nullable' => false,
            ],
            'Request data'
        )->addColumn(
            'request_data',
            Table::TYPE_TEXT,
            Table::MAX_TEXT_SIZE,
            [
                'nullable' => false,
            ],
            'Request data'
        )->addColumn(
            'response_data',
            Table::TYPE_TEXT,
            Table::MAX_TEXT_SIZE,
            [
                'nullable' => true,
            ],
            'Response data'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => TABLE::TIMESTAMP_INIT
            ],
            'Created at'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => TABLE::TIMESTAMP_INIT_UPDATE
            ],
            'Updated at'
        )->setComment(
            'OCA EPack api service tracking history'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
