<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Plugin\AdvancedReviewAndReminder\Model\Provider;

use Plumrocket\SocialLoginFree\Model\Account\Photo;

class CustomerLogoPlugin
{
    /**
     * @var \Plumrocket\SocialLoginFree\Model\Account\Photo
     */
    private $photo;

    /**
     * CustomerLogoPlugin constructor.
     *
     * @param \Plumrocket\SocialLoginFree\Model\Account\Photo $photo
     */
    public function __construct(
        Photo $photo
    ) {
        $this->photo = $photo;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo $subject
     * @param string                                                            $result
     * @return string
     */
    public function afterGenerateLogoUrl(
        \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo $subject,
        $result
    ) {
        if ($customerId = $subject->getProcessCustomerId()) {
            $customerPhoto = $this->photo->getPhotoUrl($customerId);

            if ($customerPhoto) {
                return $customerPhoto;
            }
        }

        return $result;
    }
}
