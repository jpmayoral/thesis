<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Plugin\AdvancedReviewAndReminder\Model\Provider;

class SocialButtonsPlugin
{
    /**
     * @var \Plumrocket\SocialLoginFree\Api\Buttons\ProviderInterface
     */
    private $buttonsProvider;

    public function __construct(\Plumrocket\SocialLoginFree\Api\Buttons\ProviderInterface $buttonsProvider)
    {
        $this->buttonsProvider = $buttonsProvider;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\SocialButtons $subject
     * @param                                                                    $result
     * @return array
     */
    public function afterGetList( //@codingStandardsIgnoreLine
        \Plumrocket\AdvancedReviewAndReminder\Model\Provider\SocialButtons $subject,
        $result
    ) {
        return array_merge($result, $this->buttonsProvider->getPreparedButtons(true, false));
    }
}
