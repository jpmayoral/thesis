<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Observer;

use Plumrocket\SocialLoginFree\Helper\Config;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Plumrocket\SocialLoginFree\Model\Success\RedirectManager;

/**
 * Modify after login redirect according to the configuration
 */
class LoginObserver implements ObserverInterface
{
    /**
     * @var \Plumrocket\SocialLoginFree\Helper\Config
     */
    private $config;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Plumrocket\SocialLoginFree\Model\Success\RedirectManager
     */
    private $successRedirectManager;

    /**
     * LoginObserver constructor.
     *
     * @param \Plumrocket\SocialLoginFree\Helper\Config                 $config
     * @param \Magento\Customer\Model\Session                           $customerSession
     * @param \Plumrocket\SocialLoginFree\Model\Success\RedirectManager $successRedirectManager
     */
    public function __construct(
        Config $config,
        Session $customerSession,
        RedirectManager $successRedirectManager
    ) {
        $this->config = $config;
        $this->customerSession = $customerSession;
        $this->successRedirectManager = $successRedirectManager;
    }

    public function execute(Observer $observer)
    {
        if (! $this->config->isModuleEnabled()) {
            return;
        }

        $this->customerSession->setBeforeAuthUrl($this->successRedirectManager->getAfterLoginUrl());
    }
}
