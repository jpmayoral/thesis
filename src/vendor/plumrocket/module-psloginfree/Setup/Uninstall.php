<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Setup;

use Plumrocket\Base\Setup\AbstractUninstall;
use Plumrocket\SocialLoginFree\Model\Information;
use Plumrocket\SocialLoginFree\Model\ResourceModel\Account;

class Uninstall extends AbstractUninstall
{
    protected $_configSectionId = Information::CONFIG_SECTION;
    protected $_tables = [Account::MAIN_TABLE];
    protected $_pathes = ['/app/code/Plumrocket/SocialLoginFree'];
}
