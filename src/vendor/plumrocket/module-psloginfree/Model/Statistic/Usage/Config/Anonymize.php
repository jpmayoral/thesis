<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\SocialLoginFree\Model\Statistic\Usage\Config;

class Anonymize
{
    /**
     * @param $currentValue
     * @return string|mixed
     */
    public function cmsPageValue($currentValue)
    {
        if (is_numeric($currentValue)) {
            return '__cms_page__';
        }

        return $currentValue;
    }

    /**
     * @param array $currentOptions
     * @return array
     */
    public function cmsPageOptions(array $currentOptions) : array
    {
        return array_reduce(
            $currentOptions,
            static function ($newOptions, $option) {
                if (!is_numeric($option['value'])) {
                    $newOptions[] = $option;
                }
                return $newOptions;
            },
            [['value' => '__cms_page__', 'label' => 'Some CMS page']]
        );
    }
}
