<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginPro
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Model\Network;

/**
 * Save information from network between different requests
 */
interface ApiCallParamsPersistorInterface
{
    /**
     * @param string $key
     * @param        $value
     * @return \Plumrocket\SocialLoginFree\Model\Network\ApiCallParamsPersistorInterface
     */
    public function add(string $key, $value): ApiCallParamsPersistorInterface;

    /**
     * @param array $value
     * @return $this
     */
    public function set(array $value): ApiCallParamsPersistorInterface;

    /**
     * @param string|null $key
     * @return mixed
     */
    public function get(string $key = null);

    /**
     * @return $this
     */
    public function clear(): ApiCallParamsPersistorInterface;
}
