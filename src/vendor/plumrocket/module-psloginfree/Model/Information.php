<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\SocialLoginFree\Model;

/**
 * @since 2.3.6
 */
class Information extends \Plumrocket\Base\Model\Extensions\Information
{
    const IS_SERVICE = false;
    const NAME = 'Twitter & Facebook Login';
    const WIKI = 'http://wiki.plumrocket.com/wiki/Magento_2_Twitter_and_Facebook_Login_v2.x_Extension';
    const CONFIG_SECTION = 'psloginfree';
    const MODULE_NAME = 'SocialLoginFree';
}
