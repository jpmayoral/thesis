<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Api\Buttons;

interface ProviderInterface
{
    /**
     * @param bool $onlyEnabled
     * @param null $storeId
     * @param bool $forceReload
     * @return mixed
     */
    public function getButtons($onlyEnabled = true, $storeId = null, $forceReload = false);

    /**
     * @param bool $onlyEnabled
     * @param bool $splitByVisibility
     * @param null $storeId
     * @param bool $forceReload
     * @return mixed
     */
    public function getPreparedButtons(
        $onlyEnabled = true,
        $splitByVisibility = true,
        $storeId = null,
        $forceReload = false
    );
}
