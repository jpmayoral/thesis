<?php

namespace Composer;

use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => '2.3.5-p2',
    'version' => '2.3.5.0-patch2',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'magento/project-community-edition',
  ),
  'versions' => 
  array (
    'allure-framework/allure-codeception' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d31d781b3622b028f1f6210bc76ba88438bd518',
    ),
    'allure-framework/allure-php-api' => 
    array (
      'pretty_version' => '1.1.8',
      'version' => '1.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5ae2deac1c7e1b992cfa572167370de45bdd346d',
    ),
    'allure-framework/allure-phpunit' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '45504aeba41304cf155a898fa9ac1aae79f4a089',
    ),
    'amzn/amazon-pay-and-login-magento-2-module' => 
    array (
      'pretty_version' => '3.4.1-p2',
      'version' => '3.4.1.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'amzn/amazon-pay-and-login-with-amazon-core-module' => 
    array (
      'pretty_version' => '3.4.1-p2',
      'version' => '3.4.1.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'amzn/amazon-pay-module' => 
    array (
      'pretty_version' => '3.4.1-p2',
      'version' => '3.4.1.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'amzn/amazon-pay-sdk-php' => 
    array (
      'pretty_version' => '3.6.0',
      'version' => '3.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b59f3a5b258df8a35f5f4b912a4596b3f118d7c',
    ),
    'amzn/login-with-amazon-module' => 
    array (
      'pretty_version' => '3.4.1-p2',
      'version' => '3.4.1.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'astock/stock-api-libphp' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c66a2ceea2d4c82386e3fafd1db0303fa73bedb',
    ),
    'aws/aws-sdk-php' => 
    array (
      'pretty_version' => '3.151.0',
      'version' => '3.151.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '10db635441685088731345da13c6680a3dacb003',
    ),
    'bacon/bacon-qr-code' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a91b62b9d37cee635bbf8d553f4546057250bee',
    ),
    'barbanet/magento2-argentinaregions' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '16d1f07760fa7d9034ebca870ff4607f4ae42dd7',
    ),
    'beberlei/assert' => 
    array (
      'pretty_version' => 'v2.9.9',
      'version' => '2.9.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '124317de301b7c91d5fce34c98bba2c6925bec95',
    ),
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.6.2',
      'version' => '4.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '51ac4500c4dc30cbaaabcd2f25694299df666a31',
    ),
    'blueimp/jquery-file-upload' => 
    array (
      'replaced' => 
      array (
        0 => '5.6.14',
      ),
    ),
    'braintree/braintree_php' => 
    array (
      'pretty_version' => '3.35.0',
      'version' => '3.35.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c4388199ce379432804a5c18b88585157ef2ed7',
    ),
    'cache/cache' => 
    array (
      'pretty_version' => '0.4.0',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '902b2e5b54ea57e3a801437748652228c4c58604',
    ),
    'christian-riesen/base32' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '80ff0e3b2124e61b4b39e2535709452f70bff367',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '2.4.5',
      'version' => '2.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fee32d5c82791548931cbc34806b4de6aa1abfc',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '6.8.1',
      'version' => '6.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6e94c6dadc19db05698d4e0d84214e570ce045',
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '853657f988942f7afb69becf3fd0059f192c705a',
    ),
    'colinmollenhour/cache-backend-file' => 
    array (
      'pretty_version' => 'v1.4.5',
      'version' => '1.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '03c7d4c0f43b2de1b559a3527d18ff697d306544',
    ),
    'colinmollenhour/cache-backend-redis' => 
    array (
      'pretty_version' => '1.10.6',
      'version' => '1.10.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc941a5f4cc017e11d3eab9061811ba9583ed6bf',
    ),
    'colinmollenhour/credis' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ab6db707c821055f9856b8cf76d5f44beb6fd8a',
    ),
    'colinmollenhour/php-redis-session-abstract' => 
    array (
      'pretty_version' => 'v1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '669521218794f125c7b668252f4f576eda65e1e4',
    ),
    'components/jquery' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.0',
      ),
    ),
    'components/jqueryui' => 
    array (
      'replaced' => 
      array (
        0 => '1.10.4',
      ),
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.2.8',
      'version' => '1.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a7ecad675253e4654ea05505233285377405215',
    ),
    'composer/composer' => 
    array (
      'pretty_version' => '1.10.10',
      'version' => '1.10.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '32966a3b1d48bc01472a8321fd6472b44fad033a',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6bea70230ef4dd483e6bbcab6005f682ed3a8de',
    ),
    'composer/spdx-licenses' => 
    array (
      'pretty_version' => '1.5.4',
      'version' => '1.5.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '6946f785871e2314c60b4524851f3702ea4f2223',
    ),
    'composer/xdebug-handler' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebd27a9866ae8254e873866f795491f02418c5a5',
    ),
    'container-interop/container-interop' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '79cbf1341c22ec75643d841642dd5d6acd83bdb8',
    ),
    'csharpru/vault-php' => 
    array (
      'pretty_version' => '3.5.3',
      'version' => '3.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '04be9776310fe7d1afb97795645f95c21e6b4fcf',
    ),
    'csharpru/vault-php-guzzle6-transport' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '33c392120ac9f253b62b034e0e8ffbbdb3513bd8',
    ),
    'dealerdirect/phpcodesniffer-composer-installer' => 
    array (
      'pretty_version' => 'v0.5.0',
      'version' => '0.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e749410375ff6fb7a040a68878c656c2e610b132',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.10.4',
      'version' => '1.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bfe91e31984e2ba76df1c1339681770401ec262f',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '90b2128806bfde671b6952ab8bea493942c1fdae',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f350df0268e904597e3bd9c4685c53e0e333feea',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'donatj/phpuseragentparser' => 
    array (
      'pretty_version' => 'v0.16.0',
      'version' => '0.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b3551112ed84524aef6542a8778a3f812c8098ce',
    ),
    'dotmailer/dotmailer-magento2-extension' => 
    array (
      'pretty_version' => '4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'dotmailer/dotmailer-magento2-extension-chat' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'dotmailer/dotmailer-magento2-extension-package' => 
    array (
      'pretty_version' => '4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'elasticsearch/elasticsearch' => 
    array (
      'pretty_version' => 'v7.9.0',
      'version' => '7.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1380925ccd276ab897c76596cf3705cb9714a78c',
    ),
    'endroid/qr-code' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9a57ab57ac75928fcdcfb2a71179963ff6fe573',
    ),
    'ethanyehuda/magento2-cronjobmanager' => 
    array (
      'pretty_version' => 'v1.11.3',
      'version' => '1.11.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0da72ff4fd4bb5994e02944248944d8673b6d623',
    ),
    'ezimuel/guzzlestreams' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abe3791d231167f14eb80d413420d1eab91163a8',
    ),
    'ezimuel/ringphp' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b78f89d8e0bb9e380046c31adfa40347e9f663b',
    ),
    'facebook/webdriver' => 
    array (
      'replaced' => 
      array (
        0 => '^1.7.1',
      ),
    ),
    'flow/jsonpath' => 
    array (
      'pretty_version' => '0.5.0',
      'version' => '0.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9738858c75d008c1211612b973e9510f8b7f8ea',
    ),
    'friendsofphp/php-cs-fixer' => 
    array (
      'pretty_version' => 'v2.14.6',
      'version' => '2.14.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d18a8bb180e2acde1c8031db09aefb9b73f6127',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc10d778e4b84d5bd315dad194661e091d307c6f',
    ),
    'google/recaptcha' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '614f25a9038be4f3f2da7cbfd778dc5b357d2419',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'jms/metadata' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5854ab1aa643623dc64adde718a8eec32b957a8',
    ),
    'jms/parser-lib' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c509473bc1b4866415627af0e1c6cc8ac97fa51d',
    ),
    'jms/serializer' => 
    array (
      'pretty_version' => '1.14.1',
      'version' => '1.14.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba908d278fff27ec01fb4349f372634ffcd697c0',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.2.10',
      'version' => '5.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ba9c8c862ecd5510ed16c6340aa9f6eadb4f31b',
    ),
    'khanamiryan/qrcode-detector-decoder' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '89b57f2d9939dd57394b83f6ccbd3e1a74659e34',
    ),
    'klarna/m2-payments' => 
    array (
      'pretty_version' => '7.5.1',
      'version' => '7.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'klarna/module-core' => 
    array (
      'pretty_version' => '5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'klarna/module-kp' => 
    array (
      'pretty_version' => '6.5.1',
      'version' => '6.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'klarna/module-ordermanagement' => 
    array (
      'pretty_version' => '5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'laminas/laminas-captcha' => 
    array (
      'pretty_version' => '2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b88f650f3adf2d902ef56f6377cceb5cd87b9876',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '128784abc7a0d9e1fcc30c446533aa6f1db1f999',
    ),
    'laminas/laminas-config' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '71ba6d5dd703196ce66b25abc4d772edb094dae1',
    ),
    'laminas/laminas-console' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '478a6ceac3e31fb38d6314088abda8b239ee23a5',
    ),
    'laminas/laminas-crypt' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f291fe90c84c74d737c9dc9b8f0ad2b55dc0567',
    ),
    'laminas/laminas-db' => 
    array (
      'pretty_version' => '2.11.3',
      'version' => '2.11.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c4238918b9204db1eb8cafae2c1940d40f4c007',
    ),
    'laminas/laminas-dependency-plugin' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '38bf91861f5b4d49f9a1c530327c997f7a7fb2db',
    ),
    'laminas/laminas-di' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239b22408a1f8eacda6fc2b838b5065c4cf1d88e',
    ),
    'laminas/laminas-diactoros' => 
    array (
      'pretty_version' => '1.8.7p2',
      'version' => '1.8.7.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => '6991c1af7c8d2c8efee81b22ba97024781824aaa',
    ),
    'laminas/laminas-escaper' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '25f2a053eadfa92ddacb609dcbbc39362610da70',
    ),
    'laminas/laminas-eventmanager' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1940ccf30e058b2fd66f5a9d696f1b5e0027b082',
    ),
    'laminas/laminas-feed' => 
    array (
      'pretty_version' => '2.12.3',
      'version' => '2.12.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c91415633cb1be6f9d78683d69b7dcbfe6b4012',
    ),
    'laminas/laminas-filter' => 
    array (
      'pretty_version' => '2.9.4',
      'version' => '2.9.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c4476e772a062cef7531c6793377ae585d89c82',
    ),
    'laminas/laminas-form' => 
    array (
      'pretty_version' => '2.15.0',
      'version' => '2.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '359cd372c565e18a17f32ccfeacdf21bba091ce2',
    ),
    'laminas/laminas-http' => 
    array (
      'pretty_version' => '2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '33b7942f51ce905ce9bfc8bf28badc501d3904b5',
    ),
    'laminas/laminas-hydrator' => 
    array (
      'pretty_version' => '2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a0e81cf05f32edcace817f1f48cb4055f689d85',
    ),
    'laminas/laminas-i18n' => 
    array (
      'pretty_version' => '2.10.3',
      'version' => '2.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '94ff957a1366f5be94f3d3a9b89b50386649e3ae',
    ),
    'laminas/laminas-inputfilter' => 
    array (
      'pretty_version' => '2.10.1',
      'version' => '2.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b29ce8f512c966468eee37ea4873ae5fb545d00a',
    ),
    'laminas/laminas-json' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db58425b7f0eba44a7539450cc926af80915951a',
    ),
    'laminas/laminas-loader' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d01c2c237ae9e68bec262f339947e2ea18979bc',
    ),
    'laminas/laminas-log' => 
    array (
      'pretty_version' => '2.12.0',
      'version' => '2.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e92d841b48868714a070b10866e94be80fc92ff',
    ),
    'laminas/laminas-mail' => 
    array (
      'pretty_version' => '2.12.3',
      'version' => '2.12.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c154a733b122539ac2c894561996c770db289f70',
    ),
    'laminas/laminas-math' => 
    array (
      'pretty_version' => '2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8027b37e00accc43f28605c7d8fd081baed1f475',
    ),
    'laminas/laminas-mime' => 
    array (
      'pretty_version' => '2.7.4',
      'version' => '2.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e45a7d856bf7b4a7b5bd00d6371f9961dc233add',
    ),
    'laminas/laminas-modulemanager' => 
    array (
      'pretty_version' => '2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '789bbd4ab391da9221f265f6bb2d594f8f11855b',
    ),
    'laminas/laminas-mvc' => 
    array (
      'pretty_version' => '2.7.15',
      'version' => '2.7.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e7198b03556a57fb5fd3ed919d9e1cf71500642',
    ),
    'laminas/laminas-psr7bridge' => 
    array (
      'pretty_version' => '0.2.2',
      'version' => '0.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '14780ef1d40effd59d77ab29c6d439b2af42cdfa',
    ),
    'laminas/laminas-serializer' => 
    array (
      'pretty_version' => '2.9.1',
      'version' => '2.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1c9361f114271b0736db74e0083a919081af5e0',
    ),
    'laminas/laminas-server' => 
    array (
      'pretty_version' => '2.8.1',
      'version' => '2.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4aaca9174c40a2fab2e2aa77999da99f71bdd88e',
    ),
    'laminas/laminas-servicemanager' => 
    array (
      'pretty_version' => '2.7.11',
      'version' => '2.7.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '841abb656c6018afebeec1f355be438426d6a3dd',
    ),
    'laminas/laminas-session' => 
    array (
      'pretty_version' => '2.9.3',
      'version' => '2.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '519e8966146536cd97c1cc3d59a21b095fb814d7',
    ),
    'laminas/laminas-soap' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '34f91d5c4c0a78bc5689cca2d1eaf829b27edd72',
    ),
    'laminas/laminas-stdlib' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9d84eaa39fde733356ea948cdef36c631f202b6',
    ),
    'laminas/laminas-text' => 
    array (
      'pretty_version' => '2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3601b5eacb06ed0a12f658df860cc0f9613cf4db',
    ),
    'laminas/laminas-uri' => 
    array (
      'pretty_version' => '2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6be8ce19622f359b048ce4faebf1aa1bca73a7ff',
    ),
    'laminas/laminas-validator' => 
    array (
      'pretty_version' => '2.13.4',
      'version' => '2.13.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '93593684e70b8ed1e870cacd34ca32b0c0ace185',
    ),
    'laminas/laminas-view' => 
    array (
      'pretty_version' => '2.11.4',
      'version' => '2.11.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3bbb2e94287383604c898284a18d2d06cf17301e',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4939c81f63a8a4968c108c440275c94955753b19',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9be3b16c877d477357c015cec057548cf9b2a14a',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fda190b62b962d96a069fcc414d781db66d65b69',
    ),
    'lusitanian/oauth' => 
    array (
      'pretty_version' => 'v0.8.11',
      'version' => '0.8.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc11a53db4b66da555a6a11fce294f574a8374f9',
    ),
    'magento-hackathon/magento-composer-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'magento/adobe-stock-integration' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/composer' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2dcb2194629bc2eb03fd81cb9f4586ffe4b65b0',
    ),
    'magento/framework' => 
    array (
      'pretty_version' => '102.0.5-p2',
      'version' => '102.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework-amqp' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework-bulk' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework-message-queue' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/google-shopping-ads' => 
    array (
      'pretty_version' => '4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/inventory-composer-installer' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/inventory-composer-metapackage' => 
    array (
      'pretty_version' => '1.1.5-p1',
      'version' => '1.1.5.0-patch1',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-de_de' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-en_us' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-es_es' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-fr_fr' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-nl_nl' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-pt_br' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-zh_hans_cn' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/magento-coding-standard' => 
    array (
      'pretty_version' => '5',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da46c5d57a43c950dfa364edc7f1f0436d5353a5',
    ),
    'magento/magento-composer-installer' => 
    array (
      'pretty_version' => '0.1.13',
      'version' => '0.1.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b6c32f53b4944a5d6656e86344cd0f9784709a1',
    ),
    'magento/magento2-base' => 
    array (
      'pretty_version' => '2.3.5-p2',
      'version' => '2.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/magento2-functional-testing-framework' => 
    array (
      'pretty_version' => '2.6.5',
      'version' => '2.6.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '3343a82b84e5b25d30e6e39fb3d1aa0d8dc70634',
    ),
    'magento/module-admin-analytics' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-admin-notification' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-ims' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-ims-api' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-admin-ui' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-asset' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-asset-api' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-client' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-client-api' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-image' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-image-admin-ui' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-image-api' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-advanced-pricing-import-export' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-advanced-search' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-amqp' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-amqp-store' => 
    array (
      'pretty_version' => '100.3.1',
      'version' => '100.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-analytics' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-asynchronous-operations' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-authorization' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-authorizenet' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-authorizenet-acceptjs' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-authorizenet-cardinal' => 
    array (
      'pretty_version' => '100.3.1',
      'version' => '100.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-authorizenet-graph-ql' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-backend' => 
    array (
      'pretty_version' => '101.0.5-p2',
      'version' => '101.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-backup' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-braintree' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-braintree-graph-ql' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-bundle' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-bundle-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-bundle-import-export' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cache-invalidate' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-captcha' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cardinal-commerce' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog' => 
    array (
      'pretty_version' => '103.0.5-p2',
      'version' => '103.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-cms-graph-ql' => 
    array (
      'pretty_version' => '100.3.0',
      'version' => '100.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-customer-graph-ql' => 
    array (
      'pretty_version' => '100.3.0',
      'version' => '100.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-import-export' => 
    array (
      'pretty_version' => '101.0.5-p2',
      'version' => '101.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-inventory' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-inventory-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-rule' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-rule-configurable' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-search' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-url-rewrite' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-url-rewrite-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-widget' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-checkout' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-checkout-agreements' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-checkout-agreements-graph-ql' => 
    array (
      'pretty_version' => '100.3.1',
      'version' => '100.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms' => 
    array (
      'pretty_version' => '103.0.5-p2',
      'version' => '103.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-url-rewrite' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-url-rewrite-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-config' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-import-export' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-product' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-product-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-product-sales' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-contact' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cookie' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cron' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-csp' => 
    array (
      'pretty_version' => '100.3.0',
      'version' => '100.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-currency-symbol' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer' => 
    array (
      'pretty_version' => '102.0.5-p2',
      'version' => '102.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-downloadable-graph-ql' => 
    array (
      'pretty_version' => '100.3.0',
      'version' => '100.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-import-export' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-deploy' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-developer' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-dhl' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-directory' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-directory-graph-ql' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-downloadable' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-downloadable-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-downloadable-import-export' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-eav' => 
    array (
      'pretty_version' => '102.0.5-p2',
      'version' => '102.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-eav-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-elasticsearch' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-elasticsearch-6' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-elasticsearch-7' => 
    array (
      'pretty_version' => '100.3.0',
      'version' => '100.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-email' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-encryption-key' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-fedex' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-gift-message' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-adwords' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-optimizer' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-graph-ql-cache' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-catalog-inventory' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-import-export' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-product' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-product-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-import-export' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-indexer' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-instant-purchase' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-integration' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-admin-ui' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-advanced-checkout' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-api' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-bundle-product' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-bundle-product-admin-ui' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-cache' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-admin-ui' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-api' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-search' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product-admin-ui' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product-indexer' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configuration' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configuration-api' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-distance-based-source-selection' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-distance-based-source-selection-admin-ui' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-distance-based-source-selection-api' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-elasticsearch' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-export-stock' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-export-stock-api' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-graph-ql' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-grouped-product' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-grouped-product-admin-ui' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-grouped-product-indexer' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-import-export' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-indexer' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-low-quantity-notification' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-low-quantity-notification-admin-ui' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-low-quantity-notification-api' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-multi-dimensional-indexer-api' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-product-alert' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-requisition-list' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-reservation-cli' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-reservations' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-reservations-api' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales-admin-ui' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales-api' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales-frontend-ui' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-setup-fixture-generator' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-shipping' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-shipping-admin-ui' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-source-deduction-api' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-source-selection' => 
    array (
      'pretty_version' => '1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-source-selection-api' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-layered-navigation' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-marketplace' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery' => 
    array (
      'pretty_version' => '100.3.1',
      'version' => '100.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-api' => 
    array (
      'pretty_version' => '100.3.0',
      'version' => '100.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-storage' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-message-queue' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-msrp' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-msrp-configurable-product' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-msrp-grouped-product' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-multishipping' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-mysql-mq' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-new-relic-reporting' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-newsletter' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-offline-payments' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-offline-shipping' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-page-cache' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-payment' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal-captcha' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal-graph-ql' => 
    array (
      'pretty_version' => '100.3.1',
      'version' => '100.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal-recaptcha' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-persistent' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-product-alert' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-product-video' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote' => 
    array (
      'pretty_version' => '101.1.5-p2',
      'version' => '101.1.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-related-product-graph-ql' => 
    array (
      'pretty_version' => '100.3.1',
      'version' => '100.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-release-notification' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-reports' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-require-js' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-review' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-review-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-robots' => 
    array (
      'pretty_version' => '101.0.4',
      'version' => '101.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-rss' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-rule' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales' => 
    array (
      'pretty_version' => '102.0.5-p2',
      'version' => '102.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-graph-ql' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-inventory' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-rule' => 
    array (
      'pretty_version' => '101.1.5-p2',
      'version' => '101.1.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-sequence' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sample-data' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-search' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-security' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-send-friend' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-send-friend-graph-ql' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-shipping' => 
    array (
      'pretty_version' => '100.3.5-p2',
      'version' => '100.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-signifyd' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sitemap' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-store' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-store-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swagger' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swagger-webapi' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swagger-webapi-async' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swatches' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swatches-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swatches-layered-navigation' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tax' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tax-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tax-import-export' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-theme' => 
    array (
      'pretty_version' => '101.0.5-p2',
      'version' => '101.0.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-theme-graph-ql' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tinymce-3' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-translation' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-ui' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-ups' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-url-rewrite' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-url-rewrite-graph-ql' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-user' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-usps' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-variable' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-vault' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-vault-graph-ql' => 
    array (
      'pretty_version' => '100.3.2',
      'version' => '100.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-version' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-webapi' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-webapi-async' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-webapi-security' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-weee' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-weee-graph-ql' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-widget' => 
    array (
      'pretty_version' => '101.1.4',
      'version' => '101.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-wishlist' => 
    array (
      'pretty_version' => '101.1.5-p2',
      'version' => '101.1.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-wishlist-analytics' => 
    array (
      'pretty_version' => '100.3.4',
      'version' => '100.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-wishlist-graph-ql' => 
    array (
      'pretty_version' => '100.3.3',
      'version' => '100.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/product-community-edition' => 
    array (
      'pretty_version' => '2.3.5-p2',
      'version' => '2.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/project-community-edition' => 
    array (
      'pretty_version' => '2.3.5-p2',
      'version' => '2.3.5.0-patch2',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/theme-adminhtml-backend' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/theme-frontend-blank' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/theme-frontend-luma' => 
    array (
      'pretty_version' => '100.3.5',
      'version' => '100.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/zendframework1' => 
    array (
      'pretty_version' => '1.14.4',
      'version' => '1.14.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '250f35c0e80b5e6fa1a1598c144cba2fff36b565',
    ),
    'mageplaza/magento-2-spanish-argentina-language-pack' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '031c5230dc2e57f3efa563ffdbd6bffb66675e29',
    ),
    'mageplaza/module-backend-reindex' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '90a9ab6b55106cc1732ed2b02448c725377d560e',
    ),
    'mageplaza/module-core' => 
    array (
      'pretty_version' => '1.4.9',
      'version' => '1.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c531ab1b1390ae04f6a9886d0d2f0a5d40788b9',
    ),
    'mercadopago/magento2-plugin' => 
    array (
      'pretty_version' => '3.6.0',
      'version' => '3.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a7f70c6fcfa305bca2edf8fbed879a2e932a2165',
    ),
    'mikey179/vfsstream' => 
    array (
      'pretty_version' => 'v1.6.8',
      'version' => '1.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '231c73783ebb7dd9ec77916c10037eff5a2b6efe',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.25.5',
      'version' => '1.25.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '1817faadd1846cd08be9a49e905dc68823bc38c0',
    ),
    'msp/recaptcha' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'msp/twofactorauth' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42dae2cbd13154083ca6d70099692fef8ca84bfb',
    ),
    'mustache/mustache' => 
    array (
      'pretty_version' => 'v2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e95c5a008c23d3151d59ea72484d4f72049ab7f4',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '969b211f9a51aa1f6c01d1d2aef56d3bd91598e5',
      'replaced' => 
      array (
        0 => '1.10.1',
      ),
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.7.6',
      'version' => '1.7.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f36467c7a87e20fbdc51e524fd8f9d1de80187c',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '47a1cedd2e4d52688eb8c96469c05ebc8fd28fa2',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'paragonie/sodium_compat' => 
    array (
      'pretty_version' => 'v1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbade402cbe84c69b718120911506a3aa2bae653',
    ),
    'pdepend/pdepend' => 
    array (
      'pretty_version' => '2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9daf26d0368d4a12bed1cacae1a9f3a6f0adf239',
    ),
    'pelago/emogrifier' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2472bc1c3a2dee8915ecc2256139c6100024332f',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df402786ab5368a0169091f61a7c1e0eb6852d0',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a70c0ced4be299a63d32fa96d9281d03e94041df',
    ),
    'php-amqplib/php-amqplib' => 
    array (
      'pretty_version' => 'v2.10.1',
      'version' => '2.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e2b2501e021e994fb64429e5a78118f83b5c200',
    ),
    'php-cs-fixer/diff' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '78bb099e9c16361126c86ce82ec4405ebab8e756',
    ),
    'php-webdriver/webdriver' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3308a70be084d6d7fd1ee5787b4c2e6eb4b70aab',
    ),
    'phpcollection/phpcollection' => 
    array (
      'pretty_version' => '0.5.0',
      'version' => '0.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f2bcff45c0da7c27991bbc1f90f47c4b7fb434a6',
    ),
    'phpcompatibility/php-compatibility' => 
    array (
      'pretty_version' => '9.3.5',
      'version' => '9.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9fb324479acf6f39452e0655d2429cc0d3914243',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.1',
      'version' => '5.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd870572532cd70bc3fab58f2e23ad423c8404c44',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e878a14a65245fbe78f8080eba03b47c3b705651',
    ),
    'phpmd/phpmd' => 
    array (
      'pretty_version' => '2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a05a999c644f4bc9a204846017db7bb7809fbe4c',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.7.5',
      'version' => '1.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '994ecccd8f3283ecf5ac33254543eb0ac946d525',
    ),
    'phpseclib/mcrypt_compat' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f74c7b1897b62f08f268184b8bb98d9d9ab723b0',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.28',
      'version' => '2.0.28.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1ca58cf33cb21046d702ae3a7b14fdacd9f3260',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => 'v1.10.3',
      'version' => '1.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '451c3cd1418cf640de218914901e51b064abb093',
    ),
    'phpstan/phpstan' => 
    array (
      'pretty_version' => '0.12.40',
      'version' => '0.12.40.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dce7293ad7b59fc09a9ab9b0b5b44902c092ca17',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '5.3.2',
      'version' => '5.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c89677919c5dd6d3b3852f230a663118762218ac',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '1.4.5',
      'version' => '1.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '730b01bc3e867237eaac355e06a36b85dd93a8b4',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '791198a2c6254db10131eecfe8c06670700904db',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '6.5.14',
      'version' => '6.5.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bac23fe7ff13dbdb461481f706f0e9fe746334b7',
    ),
    'phpunit/phpunit-mock-objects' => 
    array (
      'pretty_version' => '5.0.10',
      'version' => '5.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd1cf05c553ecfec36b170070573e540b67d3f1f',
    ),
    'plumrocket/module-plumbase' => 
    array (
      'pretty_version' => '2.3.6',
      'version' => '2.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'plumrocket/module-psloginfree' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '3.8.0',
      'version' => '3.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd09ea80159c1929d75b3f9c60504d613aeb4a1e3',
    ),
    'react/promise' => 
    array (
      'pretty_version' => 'v2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3cff96a19736714524ca0dd1d4130de73dbbbc4',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '3.8.0',
      ),
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '34369daee48eafb2651bea869b4b15d75ccc35f9',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '347c1d8b49c5c3ee30c7040ea6fc446790e6bddd',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd0871b3975fb7fc44d11314fd1ee20925fce4f5',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/finder-facade' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '167c45d131f7fc3d159f56f191a0a22228765e16',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/phpcpd' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dfed51c1288790fc957c9433e2f49ab152e8a564',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce990bb21759f94aeafd30209e8cfcdfa8bc3f52',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '590cfec960b77fd55e39b7d9246659e95dd6d337',
    ),
    'seld/phar-utils' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8674b1d84ffb47cc59a101f5d5a3b61e87d23796',
    ),
    'spomky-labs/otphp' => 
    array (
      'pretty_version' => 'v8.3.3',
      'version' => '8.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb14442699ae6470b29ffd89238a9ccfb9f20788',
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '3.4.2',
      'version' => '3.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8a7362af1cc1aadb5bd36c3defc4dda2cf5f0a8',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f53310646af9901292488b2ff36e26ea10f545f5',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b1d8f0d9341ea1d378b8b043ba90739f37c49d36',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '55d07021da933dd0d633ffdab6f45d5b230c7e02',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf17dc9f6ce144e41f786c32435feea4d8e11dcc',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f33a28edd42708ed579377391b3a556bcd6a626d',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '72b3a65ddd5052cf6d65eac6669748ed311f39bf',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '6140fc7047dafc5abbe84ba16a34a86c0b0229b8',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b27f491309db5757816db672b256ea2e03677d30',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '2727aa35fddfada1dd37599948528e9b152eb742',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '3675676b6a47f3e71d3ab10bcf53fb9239eb77e6',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb00d7210bc096f997e63189a62b5e35d72babac',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '376bd3a02e7946dbf90b01563361b47dde425025',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c302646f6efc070cd46856e600e5e0684d6b454',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b740103edbdcc39602239ee8860f0f45a8eb9aa5',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dcab1bc7146cf8c1beaa4502a3d9be344334251',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '37078a8dd4a2a1e9ab0231af7c6cb671b2ed5a7e',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6977d63bf9a0ad4c65cd352709e230876f9904a',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '13df84e91cd168f247c2f2ec82cc0fa24901c011',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dd93f2c578bdc9c72697eaa5f1dd25644e618d3',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '639447d008615574653fb3bc60d1986d7172eaae',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fffa1a52a023e782cdcc221d781fe1ec8f87fcca',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd87d5766cbf48d72388a9f6b85f280c8ad51f981',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '46b910c71e9828f8ec2aa7a0314de1130d9b295a',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '65e70bab62f3da7089a8d4591fb23fbacacb3479',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb617a57fc38f43bf4208dcbdb2dab3c14d9cbd9',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0c4813930953f6db6c62ebec8ee695a897b89020',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '58c7475e5457c5492c26cc740cc0ad7464be9442',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f51fb90df1154a7f75987198a9689e28f91e6a50',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f629ba9b611c76224feb21fe2bcbf0b6f992300b',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.11',
      'version' => '4.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c2d2cc66e892322cfcc03f8f12f8340dbd7a3f8a',
    ),
    'tedivm/jshrink' => 
    array (
      'pretty_version' => 'v1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '566e0c731ba4e372be2de429ef7d54f4faf4477a',
    ),
    'temando/module-shipping' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'temando/module-shipping-m2' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'temando/module-shipping-remover' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'theseer/fdomdocument' => 
    array (
      'pretty_version' => '1.6.6',
      'version' => '1.6.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e8203e40a32a9c770bcb62fe37e68b948da6dca',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'tinymce/tinymce' => 
    array (
      'replaced' => 
      array (
        0 => '3.4.7',
      ),
    ),
    'trentrichardson/jquery-timepicker-addon' => 
    array (
      'replaced' => 
      array (
        0 => '1.4.3',
      ),
    ),
    'true/punycode' => 
    array (
      'pretty_version' => 'v2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a4d0c11a36dd7f4e7cd7096076cab6d3378a071e',
    ),
    'tubalmartin/cssmin' => 
    array (
      'pretty_version' => 'v4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3cbf557f4079d83a06f9c3ff9b957c022d7805cf',
    ),
    'twbs/bootstrap' => 
    array (
      'replaced' => 
      array (
        0 => '3.1.0',
      ),
    ),
    'vertex/module-address-validation' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'vertex/module-tax' => 
    array (
      'pretty_version' => '3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'vertex/product-magento-module' => 
    array (
      'pretty_version' => '3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'vertex/sdk' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'videlalvaro/php-amqplib' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.10.1',
      ),
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v2.6.6',
      'version' => '2.6.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1d57f62db3db00d9139078cbedf262280701479',
    ),
    'webimpress/safe-writer' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cfafdec5873c389036f14bf832a5efc9390dcdd',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bafc69caeb4d49c39fd0779086c03a3738cbb389',
    ),
    'webonyx/graphql-php' => 
    array (
      'pretty_version' => 'v0.13.9',
      'version' => '0.13.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9a94fddcad0a35d4bced212b8a44ad1bc59bdf3',
    ),
    'weew/helpers-array' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bff63111f9765b4277750db8d276d92b3e16ed0',
    ),
    'wikimedia/less.php' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e238ad228d74b6ffd38209c799b34e9826909266',
    ),
    'yotpo/magento2-module-yotpo-reviews' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'yotpo/magento2-module-yotpo-reviews-bundle' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'yotpo/module-review' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'yotpo/module-yotpo' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'yubico/u2flib-server' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '55d813acf68212ad2cadecde07551600d6971939',
    ),
    'zendframework/zend-captcha' => 
    array (
      'replaced' => 
      array (
        0 => '2.9.0',
      ),
    ),
    'zendframework/zend-code' => 
    array (
      'replaced' => 
      array (
        0 => '3.3.2',
      ),
    ),
    'zendframework/zend-config' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.0',
      ),
    ),
    'zendframework/zend-console' => 
    array (
      'replaced' => 
      array (
        0 => '2.8.0',
      ),
    ),
    'zendframework/zend-crypt' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.0',
      ),
    ),
    'zendframework/zend-db' => 
    array (
      'replaced' => 
      array (
        0 => '^2.11.0',
      ),
    ),
    'zendframework/zend-di' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.1',
      ),
    ),
    'zendframework/zend-diactoros' => 
    array (
      'replaced' => 
      array (
        0 => '~1.8.7.0',
      ),
    ),
    'zendframework/zend-escaper' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.1',
      ),
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
    'zendframework/zend-feed' => 
    array (
      'replaced' => 
      array (
        0 => '^2.12.0',
      ),
    ),
    'zendframework/zend-filter' => 
    array (
      'replaced' => 
      array (
        0 => '^2.9.2',
      ),
    ),
    'zendframework/zend-form' => 
    array (
      'replaced' => 
      array (
        0 => '^2.14.3',
      ),
    ),
    'zendframework/zend-http' => 
    array (
      'replaced' => 
      array (
        0 => '^2.11.2',
      ),
    ),
    'zendframework/zend-hydrator' => 
    array (
      'replaced' => 
      array (
        0 => '2.4.2',
      ),
    ),
    'zendframework/zend-i18n' => 
    array (
      'replaced' => 
      array (
        0 => '^2.10.1',
      ),
    ),
    'zendframework/zend-inputfilter' => 
    array (
      'replaced' => 
      array (
        0 => '2.10.1',
      ),
    ),
    'zendframework/zend-json' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.1',
      ),
    ),
    'zendframework/zend-loader' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.1',
      ),
    ),
    'zendframework/zend-log' => 
    array (
      'replaced' => 
      array (
        0 => '2.12.0',
      ),
    ),
    'zendframework/zend-mail' => 
    array (
      'replaced' => 
      array (
        0 => '^2.10.0',
      ),
    ),
    'zendframework/zend-math' => 
    array (
      'replaced' => 
      array (
        0 => '2.7.1',
      ),
    ),
    'zendframework/zend-mime' => 
    array (
      'replaced' => 
      array (
        0 => '^2.7.2',
      ),
    ),
    'zendframework/zend-modulemanager' => 
    array (
      'replaced' => 
      array (
        0 => '^2.8.4',
      ),
    ),
    'zendframework/zend-mvc' => 
    array (
      'replaced' => 
      array (
        0 => '2.7.15',
      ),
    ),
    'zendframework/zend-psr7bridge' => 
    array (
      'replaced' => 
      array (
        0 => '0.2.2',
      ),
    ),
    'zendframework/zend-serializer' => 
    array (
      'replaced' => 
      array (
        0 => '2.9.1',
      ),
    ),
    'zendframework/zend-server' => 
    array (
      'replaced' => 
      array (
        0 => '2.8.1',
      ),
    ),
    'zendframework/zend-servicemanager' => 
    array (
      'replaced' => 
      array (
        0 => '2.7.11',
      ),
    ),
    'zendframework/zend-session' => 
    array (
      'replaced' => 
      array (
        0 => '^2.9.1',
      ),
    ),
    'zendframework/zend-soap' => 
    array (
      'replaced' => 
      array (
        0 => '2.8.0',
      ),
    ),
    'zendframework/zend-stdlib' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
    'zendframework/zend-text' => 
    array (
      'replaced' => 
      array (
        0 => '2.7.1',
      ),
    ),
    'zendframework/zend-uri' => 
    array (
      'replaced' => 
      array (
        0 => '2.7.1',
      ),
    ),
    'zendframework/zend-validator' => 
    array (
      'replaced' => 
      array (
        0 => '^2.13.0',
      ),
    ),
    'zendframework/zend-view' => 
    array (
      'replaced' => 
      array (
        0 => '2.11.4',
      ),
    ),
  ),
);







public static function getInstalledPackages()
{
return array_keys(self::$installed['versions']);
}









public static function isInstalled($packageName)
{
return isset(self::$installed['versions'][$packageName]);
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

$ranges = array();
if (isset(self::$installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = self::$installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}





public static function getVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['version'])) {
return null;
}

return self::$installed['versions'][$packageName]['version'];
}





public static function getPrettyVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return self::$installed['versions'][$packageName]['pretty_version'];
}





public static function getReference($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['reference'])) {
return null;
}

return self::$installed['versions'][$packageName]['reference'];
}





public static function getRootPackage()
{
return self::$installed['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
}
}
